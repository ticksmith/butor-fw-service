/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.model;

import java.util.List;

import org.butor.attrset.common.AttrSet;
import org.butor.attrset.common.AttrSetCriteria;
import org.butor.attrset.common.CodeSetCriteria;
import org.butor.attrset.common.CodeSetServices;
import org.butor.attrset.dao.AttrSetDao;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;

public class CodeSetModel implements CodeSetServices {
	private AttrSetDao _dao;
	public AttrSetDao getDao() {
		return _dao;
	}
	public void setDao(AttrSetDao dao_) {
		_dao = dao_;
	}
	@Override
	public void getCodeSet(Context ctx, CodeSetCriteria args) {
		// return only "attr set" of type "code set"
		AttrSetCriteria asc = new AttrSetCriteria();
		asc.setType("codeset");
		asc.setId(args.getId());
		asc.setK2(args.getLang());
		CommonRequestArgs cra = ctx.getRequest();
		ResponseHandler<Object> rh = ctx.getResponseHandler();
		List<AttrSet> list = _dao.getAttrSet(asc, cra);
		rh.addRow(list);
	}
	@Override
	public void getCodeSets(Context ctx, CodeSetCriteria[] args) {
		for (CodeSetCriteria asc : args) {
			getCodeSet(ctx, asc);
		}
	}
}
