/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.util;

import java.util.Collection;
import java.util.concurrent.ExecutionException;

import org.butor.attrset.common.AttrSet;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;

import com.google.common.cache.LoadingCache;

public class CachedAttributes implements Attributes {

	static  enum K {
		KEY;
	};

	final private LoadingCache<K, Attributes> attributeCache;
	
	
	CachedAttributes(LoadingCache<K, Attributes> attributeCache) {
		this.attributeCache =attributeCache;
	}

	@Override
	public Collection<AttrSet> list() {
		return getAttributes().list();
	}

	private Attributes getAttributes() {
		try {
			return attributeCache.get(K.KEY);
		} catch (ExecutionException e) {
			throw ApplicationException.exception(e, CommonMessageID.SERVICE_FAILURE.getMessage());
		}
	}

	@Override
	public String get(String k1) {
		return getAttributes().get(k1);
	}
	@Override
	public String get(String k1, String k2) {
		return getAttributes().get(k1, k2);
	}


	@Override
	public AttrSet getAttr(String k1) {
		return getAttr(k1,null);
	}
	@Override
	public AttrSet getAttr(String k1, String k2) {
		return getAttributes().getAttr(k1, k2);
	}

	@Override
	public boolean set(String k1, String k2, String value) {
		Attributes a = getAttributes();
		boolean result = a.set(k1, k2, value);
		a.save();
		return result;

	}

	@Override
	public boolean set(String k1, int seq, String value) {
		Attributes a = getAttributes();
		boolean result = a.set(k1, seq, value);
		a.save();
		return result;

	}

	@Override
	public boolean set(String k1, String value) {
		Attributes a = getAttributes();
		boolean result = a.set(k1, value);
		a.save();
		return result;
	}

	@Override
	public boolean set(String k1, String k2, int seq, String value) {
		Attributes a = getAttributes();
		boolean result = a.set(k1, k2, seq, value);
		a.save();
		return result;
	}

	@Override
	public boolean delete(String k1, String k2) {
		Attributes a = getAttributes();
		boolean result = a.delete(k1, k2);
		a.save();
		return result;
	}

	@Override
	public void save() {
		getAttributes().save();
	}

	@Override
	public void lock() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Sorry! Cached attributes do not lock.");
	}

}
