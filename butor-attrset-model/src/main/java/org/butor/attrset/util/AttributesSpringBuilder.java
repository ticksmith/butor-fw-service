/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.util;

import java.util.concurrent.TimeUnit;

import org.butor.attrset.dao.AttrSetDao;
import org.butor.attrset.util.Attributes.AttributesBuilder;
import org.butor.json.CommonRequestArgs;
import org.springframework.beans.factory.FactoryBean;

public class AttributesSpringBuilder implements FactoryBean<Attributes>{

	private Long refreshCacheSeconds;
	private String id;
	private String type;
	private AttrSetDao dao;
	private CommonRequestArgs cra;
	
	@Override
	public Attributes getObject() throws Exception {
		AttributesBuilder ab = new AttributesBuilder().setType(type).setId(id).setAttrSetDao(dao).setCommonRequestArgs(cra);
		if (refreshCacheSeconds != null) {
			ab.refreshAfter(refreshCacheSeconds, TimeUnit.SECONDS);
		}
		return ab.build();
	}

	@Override
	public Class<?> getObjectType() {
		return Attributes.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public void setRefreshCacheSeconds(Long refreshCacheSeconds) {
		this.refreshCacheSeconds = refreshCacheSeconds;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setDao(AttrSetDao dao) {
		this.dao = dao;
	}

	public void setCra(CommonRequestArgs cra) {
		this.cra = cra;
	}
	

}
