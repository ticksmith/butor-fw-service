/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.model.userProfile;

import java.util.List;

import org.butor.attrset.common.AttrSet;
import org.butor.attrset.common.AttrSetCriteria;
import org.butor.attrset.dao.AttrSetDao;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.json.service.ResponseHandlerHelper;

import com.butor.portal.common.userProfile.UserProfileModel;
import com.butor.portal.common.userProfile.UserProfileServices;

public class DefaultUserProfileModel implements UserProfileServices, UserProfileModel {
	private AttrSetDao attrSetDao;

	@Override
	public void readProfile(Context<AttrSet> ctx) {
		ResponseHandler<AttrSet> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		List<AttrSet> profile = readProfile(cra);
		ResponseHandlerHelper.addList(profile, rh);
	}

	@Override
	public void updateAttr(Context<AttrSet> ctx, AttrSet attr) {
		ResponseHandler<AttrSet> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		AttrSet as = updateAttr(attr, cra);
		rh.addRow(as);
	}

	public void setAttrSetDao(AttrSetDao attrSetDao) {
		this.attrSetDao = attrSetDao;
	}


	@Override
	public void deleteAttr(Context ctx, AttrSet attr) {
		CommonRequestArgs cra = ctx.getRequest();
		deleteAttr(attr, cra);
	}
	@Override
	public void deleteAttr(AttrSet attr, CommonRequestArgs cra) {
		AttrSetCriteria asc = new AttrSetCriteria();
		asc.setId(cra.getUserId());
		asc.setType("user");
		asc.setK1(attr.getK1());
		asc.setK2(attr.getK2());
		AttrSet as = attrSetDao.readAttrSet(asc, cra);
		if (as != null) {
			attrSetDao.deleteAttrSet(asc, cra);
		}
	}

	@Override
	public List<AttrSet> readProfile(CommonRequestArgs cra) {
		AttrSetCriteria asc = new AttrSetCriteria();
		asc.setId(cra.getUserId());
		asc.setType("user");
		return attrSetDao.getAttrSet(asc, cra);
	}

	@Override
	public AttrSet updateAttr(AttrSet attr, CommonRequestArgs cra) {
		AttrSetCriteria asc = new AttrSetCriteria();
		asc.setId(cra.getUserId());
		asc.setK1(attr.getK1());
		asc.setK2(attr.getK2());
		asc.setType("user");
		AttrSet as = attrSetDao.readAttrSet(asc, cra);
		if (as != null) {
			as.setValue(attr.getValue());
			return attrSetDao.updateAttrSet(as, cra);
		} else {
			attr.setType("user");
			return attrSetDao.insertAttrSet(attr, cra);
		}
	}
}
