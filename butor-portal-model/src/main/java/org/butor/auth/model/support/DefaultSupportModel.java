/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.model.support;

import java.util.Map;

import org.butor.auth.common.AuthModel;
import org.butor.dao.DAOMessageID;
import org.butor.dao.SqlQueryGenerator;
import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonHelper;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.butor.utils.Message;
import org.butor.utils.Message.MessageType;

import com.butor.portal.common.support.SupportServices;
import com.google.common.base.Strings;

public class DefaultSupportModel implements SupportServices {
	private AuthModel authModel;
	private SqlQueryGenerator sqlQueryGenerator;

	private String supportSysId = "portal";
	private String supportFunc = "support";
	
	@Override
	public void generateSqlQuery(Context ctx, String sqlCall) {
		CommonRequestArgs cra = ctx.getRequest();
		if (!authModel.hasAccess(supportSysId, supportFunc, AccessMode.READ, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		ResponseHandler<Object> rh = ctx.getResponseHandler();
		if (Strings.isNullOrEmpty(sqlCall)) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("sqlCall"));
		}
		/*org.butor.attrset.dao.AttrSetDaoImpl.getAttrSet({"reqId":"R-15605e6f-6677-401c-9b5b-c08c61d1ac31-0", "id":"MarketCAD", "sessionId":"SERVICE_SESSION", "userId":"jbsystem", "type":"gate", "lang":"en", "k1":null, "k2":null})*/
		int pos = sqlCall.indexOf("(");
		if (pos==-1) {
			ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("sqlCall"));
		}
		String procName = sqlCall.substring(0, pos).trim();
		int ePos = sqlCall.lastIndexOf(")");
		if (ePos == -1) {
			ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("sqlCall"));
		}
		String jsonArgs = sqlCall.substring(pos+1, ePos);
		Map<String, Object> args = new JsonHelper().deserialize(jsonArgs, Map.class);
		String sql = sqlQueryGenerator.generateQuery(procName, args);
		if (Strings.isNullOrEmpty(sql)) {
			rh.addMessage(new Message(0, MessageType.WARNING, "procedure " +procName +" either it does not exists or it was not invoked yet!"));
		}
		rh.addRow(sql);
	}

	public void setAuthModel(AuthModel authModel) {
		this.authModel = authModel;
	}

	public void setSqlQueryGenerator(SqlQueryGenerator sqlQueryGenerator) {
		this.sqlQueryGenerator = sqlQueryGenerator;
	}

	public void setSupportSysId(String supportSysId) {
		this.supportSysId = supportSysId;
	}

	public void setSupportFunc(String supportFunc) {
		this.supportFunc = supportFunc;
	}
}
