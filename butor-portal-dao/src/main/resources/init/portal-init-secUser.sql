--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--


# USER ------------------
DELETE FROM `secUser`;

# hashed version of password : titi
# "2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887"

INSERT INTO `secUser` (`id`,`active`,`firmId`,`creationDate`,`displayName`,`fullName`,`email`,`firstName`,`lastLoginDate`,`lastName`,`pwd`,`resetInProgress`,`missedLogin`,`lastQ`,`revNo`,`stamp`,`userId`) VALUES 
	('admin@this.portal',true,1,CURRENT_TIMESTAMP,'Admin','Admin','admin@this.portal','Admin',NULL,'-','2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887',false, 0, 0,1,CURRENT_TIMESTAMP,'load'),
	('portal',true,1,CURRENT_TIMESTAMP,'portal backend system user','portal backend system user','','portal',NULL,'portal','-',false, 0, 0,1,CURRENT_TIMESTAMP,'load');
