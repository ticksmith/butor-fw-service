/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common;

import org.butor.utils.Message;
import org.butor.utils.Message.MessageType;
import org.butor.utils.MessageID;

public enum AuthMessageID implements MessageID {
	MISSING_SYS(MessageType.ERROR),
	MISSING_DATA_TYPE(MessageType.ERROR),
	MISSING_DATA_FIELD(MessageType.ERROR),
	CANNOT_DELETE_REFERED_ROLE(MessageType.ERROR),
	CANNOT_DELETE_REFERED_GROUP(MessageType.ERROR),

	AUTH_SUCCESS(MessageType.INFO),
	LOGIN_SUCCESS(MessageType.INFO),
	ACCOUNT_DISABLED(MessageType.ERROR),
	ACCOUNT_DISABLED_RESET(MessageType.ERROR),
	CHANGE_PWD_SUCCESS(MessageType.INFO),
	CHANGE_PWD_FAILED(MessageType.ERROR),
	CHANGE_PWD_NOT_SUPPORTED(MessageType.ERROR),
	BAD_KAPTCHA(MessageType.ERROR),
	CHANGE_PWDS_MISMATCH(MessageType.ERROR),
	REGISTRATION_FAILED(MessageType.ERROR),
	REGISTRATION_SUCCESS(MessageType.INFO),
	USER_ALREADY_REGISTERED(MessageType.INFO),
	RESET_PWD_SUCCESS(MessageType.INFO),
	RESET_PWD_FAILED(MessageType.ERROR),
	RESET_PWD_NOT_SUPPORTED(MessageType.ERROR),
	USER_NOT_FOUND(MessageType.ERROR),
	FIRM_NOT_FOUND(MessageType.ERROR),
	AUTH_FAILED(MessageType.ERROR),
	LOGIN_FAILED(MessageType.ERROR),
	LOGIN_UNAUTHORIZED_FROM_DOMAIN(MessageType.ERROR),
	ACTIVATE_ACCOUNT_FAILED(MessageType.ERROR),
	QUESTIONS_SETUP_REQUIRED(MessageType.INFO),
	QUESTIONS_SETUP_NOT_UNIQUES(MessageType.ERROR),
	QUESTIONS_SETUP_MISSING_INFO(MessageType.ERROR),
	QUESTIONS_SETUP_FAILED(MessageType.ERROR),
	USER_ID_SHOULD_BE_EMAIL(MessageType.ERROR),
	USER_SHOULD_BE_INACTIVE_TO_BE_DELETED(MessageType.ERROR),
	FIRM_SHOULD_NOT_HAVE_USERS_TO_BE_DELETED(MessageType.ERROR),
	FIRM_SHOULD_BE_INACTIVE_TO_BE_DELETED(MessageType.ERROR),
	ACCESS_DENIED(MessageType.ERROR);

	private final static String SYSID = "auth";
	private final MessageType type;
	private final Message messageObject;

	private AuthMessageID(MessageType type) {
		this.type = type;
		this.messageObject =new Message(this); 
	}

	public Message getMessage() {
		return messageObject;
	}

	public Message getMessage(String message) {
		return new Message(this, message);
	}

	@Override
	public String getId() {
		return name();
	}


	@Override
	public String getSysId() {
		return SYSID;
	}

	@Override
	public MessageType getMessageType() {
		return type;
	}
}
