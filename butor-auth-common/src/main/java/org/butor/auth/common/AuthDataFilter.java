/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common;

import java.util.ArrayList;
import java.util.List;

public class AuthDataFilter {
	private String func;
	private String sys;
	private List<String> dataTypes = new ArrayList<String>();
	private int mode;
	private String member;

	public String getFunc() {
		return func;
	}
	public AuthDataFilter setFunc(String func) {
		this.func = func;
		return this;
	}
	public String getSys() {
		return sys;
	}
	public AuthDataFilter setSys(String sys) {
		this.sys = sys;
		return this;
	}
	public List<String> getDataTypes() {
		return dataTypes;
	}
	public AuthDataFilter setDataTypes(String... dataTypes) {
		this.dataTypes.clear();
		for (String dt : dataTypes) {
			this.dataTypes.add(dt);
		}
		return this;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataTypes == null) ? 0 : dataTypes.hashCode());
		result = prime * result + ((func == null) ? 0 : func.hashCode());
		result = prime * result + ((member == null) ? 0 : member.hashCode());
		result = prime * result + mode;
		result = prime * result + ((sys == null) ? 0 : sys.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthDataFilter other = (AuthDataFilter) obj;
		if (dataTypes == null) {
			if (other.dataTypes != null)
				return false;
		} else if (!dataTypes.equals(other.dataTypes))
			return false;
		if (func == null) {
			if (other.func != null)
				return false;
		} else if (!func.equals(other.func))
			return false;
		if (member == null) {
			if (other.member != null)
				return false;
		} else if (!member.equals(other.member))
			return false;
		if (mode != other.mode)
			return false;
		if (sys == null) {
			if (other.sys != null)
				return false;
		} else if (!sys.equals(other.sys))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AuthDataFilter [func=" + func + ", sys=" + sys + ", dataTypes=" + dataTypes + ", mode=" + mode
				+ ", member=" + member + "]";
	}
	public void setDataTypes(List<String> dataTypes) {
		this.dataTypes = dataTypes;
	}
	public int getMode() {
		return mode;
	}
	public AuthDataFilter setMode(int mode) {
		this.mode = mode;
		return this;
	}
	public String getMember() {
		return member;
	}
	public AuthDataFilter setMember(String member) {
		this.member = member;
		return this;
	}
}
