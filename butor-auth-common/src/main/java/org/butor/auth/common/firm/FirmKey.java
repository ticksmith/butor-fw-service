/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.firm;

public class FirmKey {
	private long firmId;
	private int revNo;
	public FirmKey(long firmId, int revNo) {
		super();
		this.firmId = firmId;
		this.revNo = revNo;
	}
	public long getFirmId() {
		return firmId;
	}
	public void setFirmId(long firmId) {
		this.firmId = firmId;
	}
	public int getRevNo() {
		return revNo;
	}
	public void setRevNo(int revNo) {
		this.revNo = revNo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (firmId ^ (firmId >>> 32));
		result = prime * result + revNo;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FirmKey other = (FirmKey) obj;
		if (firmId != other.firmId)
			return false;
		if (revNo != other.revNo)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CieKey [firmId=" + firmId + ", revNo=" + revNo + "]";
	}
}