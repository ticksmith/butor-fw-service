/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.auth;

import java.util.List;

public class ListAuthCriteria {
	private List<String> whoTypes;
	private List<String> whos;
	private List<String> whatTypes;
	private List<String> whats;
	private String funcSys;
	private Boolean withData;
	public String getFuncSys() {
		return funcSys;
	}
	public void setFuncSys(String funcSys) {
		this.funcSys = funcSys;
	}
	public List<String> getWhoTypes() {
		return whoTypes;
	}
	public void setWhoTypes(List<String> whoTypes) {
		this.whoTypes = whoTypes;
	}
	public List<String> getWhos() {
		return whos;
	}
	public void setWhos(List<String> whos) {
		this.whos = whos;
	}
	public List<String> getWhats() {
		return whats;
	}
	public void setWhats(List<String> whats) {
		this.whats = whats;
	}
	@Override
	public String toString() {
		return "ListAuthCriteria [whoTypes=" + whoTypes + ", whos=" + whos
				+ ", whatTypes=" + whatTypes + ", whats=" + whats
				+ ", funcSys=" + funcSys + ", withData=" + withData + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((funcSys == null) ? 0 : funcSys.hashCode());
		result = prime * result
				+ ((whatTypes == null) ? 0 : whatTypes.hashCode());
		result = prime * result + ((whats == null) ? 0 : whats.hashCode());
		result = prime * result
				+ ((whoTypes == null) ? 0 : whoTypes.hashCode());
		result = prime * result + ((whos == null) ? 0 : whos.hashCode());
		result = prime * result
				+ ((withData == null) ? 0 : withData.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListAuthCriteria other = (ListAuthCriteria) obj;
		if (funcSys == null) {
			if (other.funcSys != null)
				return false;
		} else if (!funcSys.equals(other.funcSys))
			return false;
		if (whatTypes == null) {
			if (other.whatTypes != null)
				return false;
		} else if (!whatTypes.equals(other.whatTypes))
			return false;
		if (whats == null) {
			if (other.whats != null)
				return false;
		} else if (!whats.equals(other.whats))
			return false;
		if (whoTypes == null) {
			if (other.whoTypes != null)
				return false;
		} else if (!whoTypes.equals(other.whoTypes))
			return false;
		if (whos == null) {
			if (other.whos != null)
				return false;
		} else if (!whos.equals(other.whos))
			return false;
		if (withData == null) {
			if (other.withData != null)
				return false;
		} else if (!withData.equals(other.withData))
			return false;
		return true;
	}
	public List<String> getWhatTypes() {
		return whatTypes;
	}
	public void setWhatTypes(List<String> whatTypes) {
		this.whatTypes = whatTypes;
	}
	public Boolean getWithData() {
		return withData;
	}
	public void setWithData(Boolean withData) {
		this.withData = withData;
	}
}
