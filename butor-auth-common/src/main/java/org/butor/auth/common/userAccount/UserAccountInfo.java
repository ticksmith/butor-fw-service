/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.userAccount;

import java.util.List;

import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.user.User;

/**
 * @author asawan
 *
 */
public class UserAccountInfo {
	private User user;
	private List<String> groups;
	private List<Auth> auths;
	private String clientsFirmName;
	
	private boolean resetLogin;
	private String resetLoginUrl;
	private boolean resetQuestions;
	private boolean resetAndSendLink;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<String> getGroups() {
		return groups;
	}
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}
	public List<Auth> getAuths() {
		return auths;
	}
	public void setAuths(List<Auth> auths) {
		this.auths = auths;
	}
	public String getClientsFirmName() {
		return clientsFirmName;
	}
	public void setClientsFirmName(String clientsFirmName) {
		this.clientsFirmName = clientsFirmName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((auths == null) ? 0 : auths.hashCode());
		result = prime * result + ((clientsFirmName == null) ? 0 : clientsFirmName.hashCode());
		result = prime * result + ((groups == null) ? 0 : groups.hashCode());
		result = prime * result + (resetAndSendLink ? 1231 : 1237);
		result = prime * result + (resetLogin ? 1231 : 1237);
		result = prime * result + (resetQuestions ? 1231 : 1237);
		result = prime * result + ((resetLoginUrl == null) ? 0 : resetLoginUrl.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAccountInfo other = (UserAccountInfo) obj;
		if (auths == null) {
			if (other.auths != null)
				return false;
		} else if (!auths.equals(other.auths))
			return false;
		if (clientsFirmName == null) {
			if (other.clientsFirmName != null)
				return false;
		} else if (!clientsFirmName.equals(other.clientsFirmName))
			return false;
		if (groups == null) {
			if (other.groups != null)
				return false;
		} else if (!groups.equals(other.groups))
			return false;
		if (resetAndSendLink != other.resetAndSendLink)
			return false;
		if (resetLogin != other.resetLogin)
			return false;
		if (resetQuestions != other.resetQuestions)
			return false;
		if (resetLoginUrl == null) {
			if (other.resetLoginUrl != null)
				return false;
		} else if (!resetLoginUrl.equals(other.resetLoginUrl))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "UserAccountInfo [user=" + user + ", groups=" + groups + ", auths=" + auths + ", clientsFirmName="
				+ clientsFirmName + ", resetLogin=" + resetLogin + ", resetLoginUrl=" + resetLoginUrl + ", resetQuestions="
				+ resetQuestions + ", resetAndSendLink=" + resetAndSendLink + "]";
	}
	public String getResetLoginUrl() {
		return resetLoginUrl;
	}
	public void setResetLoginUrl(String resetUrl) {
		this.resetLoginUrl = resetUrl;
	}
	public boolean isResetQuestions() {
		return resetQuestions;
	}
	public void setResetQuestions(boolean resetQuestions) {
		this.resetQuestions = resetQuestions;
	}
	public boolean isResetAndSendLink() {
		return resetAndSendLink;
	}
	public void setResetAndSendLink(boolean resetAndSendLink) {
		this.resetAndSendLink = resetAndSendLink;
		this.resetLogin = true;
	}
	public boolean isResetLogin() {
		return resetLogin;
	}
	public void setResetLogin(boolean resetLogin) {
		this.resetLogin = resetLogin;
	}
}
