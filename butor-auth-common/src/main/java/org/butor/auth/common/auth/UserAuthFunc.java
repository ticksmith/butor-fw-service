/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.auth;



public class UserAuthFunc extends Auth {
	private String funcSys;
	private String func;
	public String getFuncSys() {
		return funcSys;
	}
	public void setFuncSys (String funcSys) {
		this.funcSys = funcSys;
	}
	public String getFunc() {
		return func;
	}
	public void setFunc(String func) {
		this.func = func;
	}
	@Override
	public String toString() {
		return "UserAuthFunc [funcSys=" + funcSys + ", func=" + func
				+ ", getWhoType()=" + getWhoType() + ", getWho()=" + getWho()
				+ ", getWhatType()=" + getWhatType() + ", getWhat()="
				+ getWhat() + ", getSys()=" + getSys() + ", getMode()="
				+ getMode() + ", getStartDate()=" + getStartDate()
				+ ", getEndDate()=" + getEndDate() + ", getStamp()="
				+ getStamp() + ", getRevNo()=" + getRevNo() + ", getUserId()="
				+ getUserId() + ", toString()=" + super.toString()
				+ ", hashCode()=" + hashCode() + ", getAuthId()=" + getAuthId()
				+ ", getData()=" + getData() + ", getDataId()=" + getDataId()
				+ ", getClass()=" + getClass() + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((func == null) ? 0 : func.hashCode());
		result = prime * result + ((funcSys == null) ? 0 : funcSys.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAuthFunc other = (UserAuthFunc) obj;
		if (func == null) {
			if (other.func != null)
				return false;
		} else if (!func.equals(other.func))
			return false;
		if (funcSys == null) {
			if (other.funcSys != null)
				return false;
		} else if (!funcSys.equals(other.funcSys))
			return false;
		return true;
	}
	

}
