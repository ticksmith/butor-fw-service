/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.firm;

import org.butor.json.service.Context;
import org.butor.utils.AccessMode;

public interface FirmServices {
	void listFirm(Context<FirmWithAccessMode> ctx, ListFirmCriteria crit);

	void readFirm(Context<Firm> ctx, long firmId, String sys, String func, AccessMode mode);

	void insertFirm(Context<FirmKey> ctx, Firm firm);

	void updateFirm(Context<FirmKey> ctx, Firm firm);

	void deleteFirm(Context<Firm> ctx, FirmKey firmKey);
}