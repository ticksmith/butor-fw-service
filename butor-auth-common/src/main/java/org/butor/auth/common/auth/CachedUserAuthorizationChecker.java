/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.auth;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.butor.auth.common.AuthServices;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandlerHelper;
import org.butor.json.util.ContextBuilder;
import org.butor.json.util.JsonResponse;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader.InvalidCacheLoadException;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

/**
 * 
 * @author asawan
 */
public class CachedUserAuthorizationChecker implements UserAuthorizationChecker {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private AuthServices authServices;
	
	private Cache<String, Boolean> authCache;

	public CachedUserAuthorizationChecker(AuthServices authServices) {
		this(authServices, 3, "MINUTES");
	}
	public CachedUserAuthorizationChecker(AuthServices authServices, int timeout, String timeUnit) {
		this.authServices = Preconditions.checkNotNull(authServices);
		authCache = CacheBuilder.newBuilder()
			.expireAfterWrite(timeout, TimeUnit.valueOf(timeUnit))
			.removalListener(new RemovalListener<String, Boolean>() {
				@Override
				public void onRemoval(RemovalNotification<String, Boolean> notification) {
					logger.info("Removing item from cache. user.sys.func.mode : {} cause : {}",
						notification.getKey(),
						notification.getCause()); 
				}
			})
			.build();
	}

	protected Boolean getCachedAuth(final String username, final String sys, 
			final String func, final AccessMode mode) {
		try {
			String key = String.format("%s.%s.%s.%s", username, sys, func, mode.value());
			return authCache.get(key, new Callable<Boolean>() {
				@Override
				public Boolean call() throws Exception {
					logger.info("Loading auth in cache : username:{}, sys:{}, func:{}, mode:{}", 
						new Object[]{username, sys, func, mode.value()});
					JsonResponse<Boolean> handler = ResponseHandlerHelper.createJsonResponse(Boolean.class);
					Context<Boolean> ctx = new ContextBuilder<Boolean>()
						.createCommonRequestArgs(username, "en")
						.setResponseHandler(handler)
						.build();

					authServices.hasAccess(ctx, sys, func, mode);
					return handler.getRow();
				}
			});
		} catch (InvalidCacheLoadException e) {
			// no entry found by call()!
			return null;
		} catch (ExecutionException e) {
			throw ApplicationException.exception(e, CommonMessageID.SERVICE_FAILURE.getMessage());
		}
	}

	@Override
	public boolean hasAccess(String username, String sys, String func) {
		return hasAccess(username, sys, func, AccessMode.READ);
	}
	@Override
	public boolean hasAccess(String username, String sys, String func, AccessMode am) {
		Boolean granted = getCachedAuth(username, sys, func, am);
		return granted != null && granted.booleanValue();
	}
}