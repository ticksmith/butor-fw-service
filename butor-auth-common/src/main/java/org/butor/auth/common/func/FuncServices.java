/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.func;

import org.butor.json.service.Context;

public interface FuncServices {
	void readFunc(Context<Func> ctx, FuncKey funcKey);
	void insertFunc(Context<Func> ctx, Func func);
	void deleteFunc(Context<Func> ctx, FuncKey funcKey);
	void updateFunc(Context<Func> ctx, Func func);
	void listFunc(Context<Func> ctx, String sys, String func);
}
