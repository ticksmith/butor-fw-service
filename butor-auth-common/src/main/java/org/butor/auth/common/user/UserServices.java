/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.user;

import org.butor.json.service.Context;

public interface UserServices {
	
	void readUser(Context<User> ctx, String id, String func);
	void insertUser(Context<UserKey> ctx, User user);
	void updateUser(Context<UserKey> ctx, User user);
	void deleteUser(Context<Void> ctx, UserKey userKey);

	void listUser(Context<User> ctx, ListUserCriteria criteria, String func);
	
	void readQuestions(Context<UserQuestions> ctx, String id);
	void updateQuestions(Context<UserKey> ctx, UserQuestions questions);

	void updateState(Context<UserKey> ctx, User user);

	void resetLogin(Context<String> ctx, String id, String domain, boolean resetAndSendLink);
	void resetLogin(Context<String> ctx, String id, String domain, boolean resetQuestions, boolean resetAndSendLink);
}
