/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.role;

import org.butor.auth.common.func.FuncKey;
import org.butor.json.service.Context;

public interface RoleServices {
	void readRole(Context<Role> ctx, String roleId);
	void deleteRole(Context<Role> ctx, RoleKey roleKey);
	void createRole(Context<Role> ctx, Role role);
	void updateRole(Context<Role> ctx, Role role);
	void listRole(Context<Role> ctx, FuncKey criteria, String func);
}
