/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.user;

import org.butor.json.service.Context;
/**
 * This class is useful when you need to override some behavior of a UserServices
 * It uses the decorator pattern and forward all the call to the class returned by delegate().
 * 
 * It's a popular pattern used in Guava and other frameworks
 * 
 * @author tbussier
 *
 */
public abstract class ForwardingUserServices implements UserServices{

	
	protected abstract UserServices delegate();
	
	@Override
	public void readUser(Context<User> ctx, String id, String func) {
		delegate().readUser(ctx, id, func);
	}

	@Override
	public void insertUser(Context<UserKey> ctx, User user) {
		delegate().insertUser(ctx, user);
	}

	@Override
	public void updateUser(Context<UserKey> ctx, User user) {
		delegate().updateUser(ctx, user);
	}

	@Override
	public void deleteUser(Context<Void> ctx, UserKey userKey) {
		delegate().deleteUser(ctx, userKey);
	}

	@Override
	public void listUser(Context<User> ctx, ListUserCriteria criteria, String func) {
		delegate().listUser(ctx, criteria, func);
	}

	@Override
	public void readQuestions(Context<UserQuestions> ctx, String id) {
		delegate().readQuestions(ctx, id);
	}

	@Override
	public void updateQuestions(Context<UserKey> ctx, UserQuestions questions) {
		delegate().updateQuestions(ctx, questions);
	}

	@Override
	public void updateState(Context<UserKey> ctx, User user) {
		delegate().updateState(ctx, user);
	}
	
	@Override
	public void resetLogin(Context<String> ctx, String id, String domain, boolean resetAndSendLink) {
		delegate().resetLogin(ctx, id, domain, resetAndSendLink);
	}
	
	@Override
	public void resetLogin(Context<String> ctx, String id, String domain, boolean resetQuestions,
			boolean resetAndSendLink) {
		delegate().resetLogin(ctx, id, domain, resetAndSendLink);
	}
}
