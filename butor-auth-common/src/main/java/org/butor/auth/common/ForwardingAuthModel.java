/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common;

import java.util.List;

import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.auth.AuthKey;
import org.butor.auth.common.auth.ListAuthCriteria;
import org.butor.auth.common.auth.SecData;
import org.butor.auth.common.func.Func;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.AccessMode;

/**
 * This class is useful when you need to override some behavior of a AuthModel
 * It uses the decorator pattern and forward all the call to the class returned by delegate().
 * 
 * It's a popular pattern used in Guava and other frameworks
 * 
 * @author tbussier
 *
 */
public abstract class ForwardingAuthModel implements AuthModel {
	
	protected abstract AuthModel delegate();

	@Override
	public boolean hasAccess(String system, String func, AccessMode am, CommonRequestArgs cra) {
		return delegate().hasAccess(system, func, am, cra);
	}

	@Override
	public List<Func> listAuthFunc(CommonRequestArgs cra) {
		return delegate().listAuthFunc(cra);
	}

	@Override
	public List<AuthData> listAuthData(ListAuthDataCriteria criteria, CommonRequestArgs cra) {
		return delegate().listAuthData(criteria, cra);
	}

	@Override
	public Auth readAuth(int authId, CommonRequestArgs cra) {
		return delegate().readAuth(authId, cra);
	}

	@Override
	public AuthKey createAuth(Auth auth, CommonRequestArgs cra) {
		return delegate().createAuth(auth, cra);
	}

	@Override
	public void deleteAuth(AuthKey ak, CommonRequestArgs cra) {
		delegate().deleteAuth(ak, cra);
	}

	@Override
	public AuthKey updateAuth(Auth auth, CommonRequestArgs cra) {
		return delegate().updateAuth(auth, cra);
	}

	@Override
	public List<Auth> listAuth(ListAuthCriteria criteria, CommonRequestArgs cra) {
		return delegate().listAuth(criteria, cra);
	}

	@Override
	public List<SecData> listData(SecData criteria, CommonRequestArgs cra) {
		return delegate().listData(criteria, cra);
	}

	@Override
	public void updateData(long dataId, List<SecData> data, CommonRequestArgs cra) {
		delegate().updateData(dataId, data, cra);
	}
}
