/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mule.component;

import javax.activation.DataHandler;

import org.butor.utils.ApplicationException;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;

import com.google.api.client.util.Preconditions;

/**
 * @author asawan
 *
 */
public class ButorDataHandlerComponent implements Callable {
	private ButorDataHandler handler;
	
	public ButorDataHandlerComponent(ButorDataHandler handler) {
		this.handler = Preconditions.checkNotNull(handler);
	}
	@Override
	public Object onCall(MuleEventContext context) throws Exception {
		final MuleMessage mmsg = context.getMessage();
		
		if (!(mmsg.getPayload() instanceof DataHandler)) {
			ApplicationException.exception("Not an DataHandler object!");
		}

		DataHandler dh = (DataHandler)mmsg.getPayload();
		return handler.handleData(dh);
	}
}