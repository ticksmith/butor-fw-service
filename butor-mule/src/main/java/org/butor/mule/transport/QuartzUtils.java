/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mule.transport;

import java.util.Collection;
import java.util.Date;

import org.butor.utils.ApplicationException;
import org.butor.utils.CommonDateFormat;
import org.butor.utils.CommonMessageID;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.SchedulerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class QuartzUtils {
	final private static Logger logger = LoggerFactory.getLogger(QuartzUtils.class);
	public static boolean isDayLastRun(String jobName) throws Exception {
		boolean isNextRunSameDay = false;
		String today = CommonDateFormat.YYYYMMDD.format(new Date());
		Date nextRunTime = getNextRunTime(jobName);
		if (nextRunTime != null) {
			isNextRunSameDay = CommonDateFormat.YYYYMMDD.format(nextRunTime).equals(today);
		}
		return !isNextRunSameDay;
	}
	public static Date getNextRunTime(String jobName) throws Exception {
		SchedulerRepository sr = SchedulerRepository.getInstance();
		Collection<Scheduler> sc = sr.lookupAll();
		if (sc == null) {
			ApplicationException.exception(CommonMessageID.NOT_FOUND.getMessage("Scheduler"));
		}
		Trigger[] tl = null;
		for (Scheduler s : sc) {
			String[] gnl = s.getJobGroupNames();
			for (String gn : gnl) {
				tl = s.getTriggersOfJob("quartz://" +jobName, gn);
				if (tl != null && tl.length > 0) {
					break;
				}
			}
			if (tl != null && tl.length > 0) {
				break;
			}
		}
		
		if (tl != null && tl.length > 0) {
			return tl[0].getNextFireTime();
		} else {
			logger.warn("Did not found schedule of quartz job: " +jobName);
		}
		return null;
	}
}
