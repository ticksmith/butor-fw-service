/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.notif.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.butor.notif.AbstractNotifRelay;
import com.google.common.base.Preconditions;
import com.google.common.eventbus.EventBus;

/**
 * 
 * @author asawan
 * 
 */
public class JMSTopicNotifRelay extends AbstractNotifRelay implements MessageListener {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private JmsTemplate jmsTemplate;
	
	public JMSTopicNotifRelay(EventBus eventBus, JmsTemplate jmsTemplate) {
		super(eventBus);
		this.jmsTemplate = Preconditions.checkNotNull(jmsTemplate);
	}
	
	/**
	 * handle incoming from topic notification message
	 */
	@Override
	public void onMessage(Message message) {
		try {
			TextMessage tm = (TextMessage) message;
			processInboundMessage(tm.getText());
		} catch (Exception e) {
			logger.warn("failed while processing inbound jms message!");
		}
	}
	/**
	 * post into topic a notification message
	 */
	@Override
	public void postMessage(final String serializedClientNotif) {
		if (serializedClientNotif == null) {
			return;
		}
		try {
			jmsTemplate.send(new MessageCreator() {
				public Message createMessage(Session session) throws JMSException {
					TextMessage message = session.createTextMessage(serializedClientNotif);
					logger.info("Sending message " +serializedClientNotif);
					return message;
				}
			});
		} catch (Exception e) {
			logger.warn("failed while processing outbound jms message!");
		}
	}
}
