/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.report;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.butor.json.JsonHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractTableModel implements TableModel {
	private Logger logger = LoggerFactory.getLogger(getClass());

	private List<Map<String, String>> fields;
	private Map<String, Integer> indexMap;
	private BundleModel bundle;
	public AbstractTableModel(String fieldsJson, BundleModel bundle) {
		this.fields = new JsonHelper().deserialize(fieldsJson, List.class);
		this.bundle = bundle;
		this.indexMap = new HashMap<String, Integer>();
		for (int c=0; c<fields.size(); c+=1) {
			Map<String, String> field = fields.get(c);
			indexMap.put(field.get("name"), Integer.valueOf(c));
		}
	}
	@Override
	public void preProcessingRow(Object rec) {
		//ok
	}
	@Override
	public int getColumnsCount() {
		return fields.size();
	}

	@Override
	public String getHeader(int index, String lang) {
		String name = getColumnName(index);
		if (name != null) {
			return bundle.get(name, lang);
		}
		logger.warn("bundle not found for column=" +index);
		return "";
	}

	@Override
	public String getColumnName(int index) {
		Map<String, String> field = fields.get(index);
		return field.get("name");
	}

	@Override
	public int getColumnIndex(String name) {
		return indexMap.get(name);
	}

	@Override
	public int getColumnWidth(int index, String lang) {
		Map<String, String> field = fields.get(index);
		String val = field.get("width");
		return isNullOrEmpty(val) ? -1 : Integer.valueOf(val).intValue();
	}

	@Override
	public Object getValue(Object rec, int index, String lang) {
		Map<String, String> field = fields.get(index);
		String name = field.get("name");
		try {
			return PropertyUtils.getProperty(rec, name);
		} catch (Throwable e) {
			logger.warn("Failed to get value of " +name, e);
			return null;
		}
	}
	
	@Override
	public boolean hasTotalRow() {
		return false;
	}
	@Override
	public int[] getTotalColumns() {
		return null;
	}
}
