/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mule.common;

import java.util.Date;

public class ServiceCall {
	private String user;
	private String service;
	private String namespace;
	private String payload;
	private boolean success;
	private long duration;
	private Date serviceCallTimestamp;
	private Date stamp;
	private int revNo;
	private String userId;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getStamp() {
		return stamp;
	}
	public void setStamp(Date stamp) {
		this.stamp = stamp;
	}
	public int getRevNo() {
		return revNo;
	}
	public void setRevNo(int revNo) {
		this.revNo = revNo;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Date getServiceCallTimestamp() {
		return serviceCallTimestamp;
	}
	public void setServiceCallTimestamp(Date serviceCallTimestamp) {
		this.serviceCallTimestamp = serviceCallTimestamp;
	}
	
	@Override
	public String toString() {
		return "ServiceCall [user=" + user + ", service=" + service
				+ ", payload=" + payload + ", success=" + success
				+ ", duration=" + duration + ", serviceCallTimestamp="
				+ serviceCallTimestamp + ", stamp=" + stamp + ", revNo=" + revNo
				+ ", userId=" + userId + "]";
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
}
