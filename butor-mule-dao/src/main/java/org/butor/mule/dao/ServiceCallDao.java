/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mule.dao;

import java.util.List;

import org.butor.json.CommonRequestArgs;
import org.butor.mule.common.ServiceCall;

public interface ServiceCallDao {
	void insertServiceCall(ServiceCall criteria, CommonRequestArgs commonRequestArgs);
	
	List<ServiceCall> listServiceCalls(CommonRequestArgs commonRequestArgs);
	
	List<ServiceCall> listServiceCallsByUser(String user, CommonRequestArgs commonRequestArgs);

	List<ServiceCall> listServiceCallsByService(String service, CommonRequestArgs commonRequestArgs);

}
