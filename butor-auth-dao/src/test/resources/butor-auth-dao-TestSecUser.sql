--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--


insert INTO secUser (`id`,`active`,`firmId`,`creationDate`,`displayName`,`fullName`,`email`,`firstName`,`lastLoginDate`,`lastName`,`pwd`,`resetInProgress`,`missedLogin`,`lastQ`,`revNo`,`stamp`,`userId`) VALUES 
	('unitTest1',true,2,CURRENT_TIMESTAMP,'unitTest1','unitTest1','unitTest1@test.tst','unitTest1',now(),'unitTest1','2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887',false, 0, 0,1,CURRENT_TIMESTAMP,'load'),
	('unitTest2',false,1,CURRENT_TIMESTAMP,'unitTest2','unitTest2','unitTest2@test.tst','unitTest2',now(),'unitTest2','2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887',false, 0, 0,1,CURRENT_TIMESTAMP,'load'),
	('unitTest3',true,1,CURRENT_TIMESTAMP,'unitTest3','unitTest3','unitTest3@test.tst','unitTest3',now(),'unitTest3','2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887',false, 0, 0,1,CURRENT_TIMESTAMP,'load'),
	('unitTest4',true,5,CURRENT_TIMESTAMP,'unitTest4','unitTest4','unitTest4@test.tst','unitTest4',now(),'unitTest4','2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887',false, 0, 0,1,CURRENT_TIMESTAMP,'load'),
	('unitTest5',false,6,CURRENT_TIMESTAMP,'unitTest5','unitTest5','unitTes5t@test.tst','unitTest5',now(),'unitTest5','2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887',false, 0, 0,1,CURRENT_TIMESTAMP,'load');

update secUser
set q1 = '', q2 = '', q3 = '', r1='', r2='', r3='';
	
INSERT INTO secAuth (`authId`,`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	(2,'unitTest2','user','users','func','sec',2,2,null,null,NOW(),'load',0),
	(3,'unitTest2','user','test1','func','sec',2,2,null,null,NOW(),'load',0),
	(4,'unitTest2','user','test2','func','sec',2,2,null,null,NOW(),'load',0);

INSERT INTO secGroup (`groupId`,`member`,`stamp`,`userId`,`revNo`) VALUES
	('testGroup','unitTest2',NOW(),'load',0),
	('testGroup2','unitTest2',NOW(),'load',0);


INSERT INTO secData (`dataId`,`sys`,`dataType`,`d1`,`stamp`,`userId`,`revNo`) VALUES
	(2,'sec','user','unitTest2',NOW(),'load',0);

