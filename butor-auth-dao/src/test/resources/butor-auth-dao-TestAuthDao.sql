--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

truncate secDesc;
truncate secFunc;
truncate secGroup;
truncate secRole;
truncate secData;
truncate secAuth;
truncate secUser;

INSERT INTO secUser (`id`,`active`,`firmId`,`creationDate`,`displayName`,`fullName`,`email`,`firstName`,`lastLoginDate`,`lastName`,`pwd`,`resetInProgress`,`missedLogin`,`lastQ`,`revNo`,`stamp`,`userId`) VALUES 
	('unitTest',true,1,CURRENT_TIMESTAMP,'unitTest','unitTest','unitTest@test.tst','unitTest',now(),'unitTest','2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887',false, 0, 0,1,CURRENT_TIMESTAMP,'load');

INSERT INTO secFunc (`func`, `sys`, `description`, `stamp`, `userId`,`revNo`) VALUES 
	('sec','sec','Security', NOW(), 'load',0),
	('firms','sec','Security firms', NOW(), 'load',0),
	('users','sec','Security users', NOW(), 'load',0),
	('funcs','sec','Security functions', NOW(), 'load',0),
	('roles','sec','Security roles', NOW(), 'load',0),
	('groups','sec','Security groups', NOW(), 'load',0),
	('auths','sec','Security authorisations', NOW(), 'load',0);

	
INSERT INTO secDesc (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','role', 'Security admin role',NOW(),'load',0);

INSERT INTO secRole (`roleId`,`func`,`sys`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','sec','sec',NOW(),'load',0),
	('sec.admin','firms','sec',NOW(),'load',0),
	('sec.admin','users','sec',NOW(),'load',0),
	('sec.admin','groups','sec',NOW(),'load',0),
	('sec.admin','funcs','sec',NOW(),'load',0),
	('sec.admin','roles','sec',NOW(),'load',0),
	('sec.admin','auths','sec',NOW(),'load',0);

INSERT INTO secDesc (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','group', 'Security admin group',NOW(),'load',0);

INSERT INTO secGroup (`groupId`,`member`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','unitTest',NOW(),'load',0);

INSERT INTO secAuth (`authId`,`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	(1,'sec.admin','group','sec.admin','role','',1,2,null,null,NOW(),'load',0);


INSERT INTO secData (`dataId`,`sys`,`dataType`,`d1`,`stamp`,`userId`,`revNo`) VALUES
	(1,'sec','firm','*',NOW(),'load',0),
	(1,'sec','user','*',NOW(),'load',0),
	(1,'sec','group','*',NOW(),'load',0),
	(1,'sec','role','*',NOW(),'load',0),
	(1,'sec','func','*',NOW(),'load',0);

	
