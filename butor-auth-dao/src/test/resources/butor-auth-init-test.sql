--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

delete FROM  `PORTAL`.`secDesc` WHERE 1=1;

delete FROM  `PORTAL`.`secFunc` WHERE 1=1;
INSERT INTO `PORTAL`.`secFunc` (`func`, `description`, `stamp`, `userId`,`revNo`) VALUES 
	('tx','TraderX', NOW(), 'load',0),
	('sec','Security', NOW(), 'load',0),
	('jb-cal','JamesBond calendar', NOW(), 'load',0),
	('jb-comm','JamesBond commission', NOW(), 'load',0),
	('jb-inventory','JamesBond inventory', NOW(), 'load',0),
	('jb-order','JamesBond order', NOW(), 'load',0),
	('jb-security','JamesBond Security', NOW(), 'load',0),
	('jb-auth','JamesBond Authorisation', NOW(), 'load',0);


delete FROM  `PORTAL`.`secGroup` WHERE 1=1;
INSERT INTO `PORTAL`.`secGroup` (`groupId`,`member`,`stamp`,`userId`,`revNo`) VALUES
	('sec-admin','aiman.sawan@gmail.com',NOW(),'load',0),
	('jb-admin','aiman.sawan@gmail.com',NOW(),'load',0),
	('jb-admin','YTherrien@IAGTO.CA',NOW(),'load',0),
	('jb-admin-cie','aiman.sawan@gmail.com',NOW(),'load',0),
	('trader','aiman.sawan@gmail.com',NOW(),'load',0),
	('trader','YTherrien@IAGTO.CA',NOW(),'load',0);


delete FROM  `PORTAL`.`secRole` WHERE 1=1;
INSERT INTO `PORTAL`.`secRole` (`roleId`,`func`,`stamp`,`userId`,`revNo`) VALUES
	('role-sec','sec', NOW(),'load',0),
	('role-trader','tx', NOW(),'load',0),
	('role-trader','jb-order', NOW(),'load',0),
	('role-trader','jb-inventory', NOW(),'load',0);



delete FROM  `PORTAL`.`secData` WHERE 1=1;
INSERT INTO `PORTAL`.`secData` (`dataId`,`sys`,`dataType`,`d1`,`d2`,`d3`,`d4`,`d5`,`stamp`,`userId`,`revNo`) VALUES
	('firm3','tx','firm','3',null,null,null,null,NOW(),'load',0),
	('firmAll','tx','firm','7',null,null,null,null,NOW(),'load',0),
	('firmAll','tx','firm','8',null,null,null,null,NOW(),'load',0),
	('firmAll','tx','firm','9',null,null,null,null,NOW(),'load',0);


delete FROM  `PORTAL`.`secAuth`;
INSERT INTO `PORTAL`.`secAuth` (`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	('sec-admin','grp','sec','role','sec',null,1,null,null,CURRENT_TIMESTAMP,'load',0),
	('jb-admin','grp','trader','role','tx',null,1,null,null,NOW(),'load',0),
	('trader','grp','sec','role','tx','firm3',1,null,null,NOW(),'load',0),
	('trader','grp','trader','role','tx','firmAll',1,timestamp('2012-02-01 11:11:11'),timestamp('2013-01-29 14:14:11'),NOW(),'load',0);

	
DELETE FROM  `PORTAL`.`secDesc` where idType='group';
INSERT INTO `PORTAL`.`secDesc` (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','group', 'Security admin group',NOW(),'load',0)
;
DELETE FROM  `PORTAL`.`secGroup`;
INSERT INTO `PORTAL`.`secGroup` (`groupId`,`member`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','aiman.sawan@gmail.com',NOW(),'load',0)
;

DELETE FROM  `PORTAL`.`secDesc` where idType='role';
INSERT INTO `PORTAL`.`secDesc` (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','role', 'Security admin role',NOW(),'load',0)
;
DELETE FROM  `PORTAL`.`secRole`;
INSERT INTO `PORTAL`.`secRole` (`roleId`,`func`,`sys`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','sec','sec',NOW(),'load',0),
	('sec.admin','firms','sec',NOW(),'load',0),
	('sec.admin','users','sec',NOW(),'load',0),
	('sec.admin','groups','sec',NOW(),'load',0),
	('sec.admin','funcs','sec',NOW(),'load',0),
	('sec.admin','roles','sec',NOW(),'load',0),
	('sec.admin','auths','sec',NOW(),'load',0)
;

DELETE FROM  `PORTAL`.`secData`;
INSERT INTO `PORTAL`.`secData` (`dataId`,`sys`,`dataType`,`d1`,`stamp`,`userId`,`revNo`) VALUES
	(1,'sec','firm','*',NOW(),'load',0),
	(1,'sec','user','*',NOW(),'load',0),
	(1,'sec','sys','*',NOW(),'load',0),
	(1,'sec','group','*',NOW(),'load',0),
	(1,'sec','role','*',NOW(),'load',0),
	(1,'sec','func','*',NOW(),'load',0),

	(2,'sec','firm','*',NOW(),'load',0);

DELETE FROM  `PORTAL`.`secAuth`;
INSERT INTO `PORTAL`.`secAuth` (`authId`,`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	(1,'sec.admin','group','sec.admin','role','',1,2,null,null,NOW(),'load',0),
	(2,'portal','user','users','func','sec',-1,2,null,null,NOW(),'load',0),
	(3,'aiman.sawan@gmail.com','user','support','func','portal',-2,2,null,null,NOW(),'load',0)
;

REPLACE INTO `PORTAL`.`secUser` (`id`,`active`,`firmId`,`creationDate`,`displayName`,`fullName`,`email`,`firstName`,`lastLoginDate`,`lastName`,`pwd`,`resetInProgress`,`missedLogin`,`lastQ`,`revNo`,`stamp`,`userId`) VALUES 
	('unitTest',true,1,CURRENT_TIMESTAMP,'unitTest','unitTest','unitTest@test.tst','unitTest',now(),'unitTest','2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887',false, 0, 0,1,CURRENT_TIMESTAMP,'load')
;

	
REPLACE INTO `PORTAL`.`secGroup` (`groupId`,`member`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','unitTest',NOW(),'load',0)
;
