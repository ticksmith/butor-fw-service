--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--


	
INSERT INTO secDesc (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('testRole1','role', 'Test role 1',NOW(),'load',0),
	('testRole2','role', 'Test role 2',NOW(),'load',0),
	('testRole3','role', 'Test role 3',NOW(),'load',0);
	
INSERT INTO secAuth (`authId`,`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	(2,'unitTest2','user','roles','func','sec',2,2,null,null,NOW(),'load',0);


INSERT INTO secData (`dataId`,`sys`,`dataType`,`d1`,d2,`stamp`,`userId`,`revNo`) VALUES
	(2,'sec','role','sec.admin','', NOW(),'load',0),
	(2,'sec','func','sec','users',NOW(),'load',0),
	(2,'sec','func','sec','auths',NOW(),'load',0);
