/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao.integration;

import static org.butor.test.ButorTestUtil.getCommonRequestArgs;
import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.auth.common.func.Func;
import org.butor.auth.common.func.FuncKey;
import org.butor.auth.dao.FuncDao;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;


@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:test-context.xml")
public class TestSecFunc {

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}

	@Resource(name="secDs")
	DataSource dataSource;

	@Resource
	FuncDao funcDao;

	ButorTestUtil tu = new ButorTestUtil();
	
	@Before
	public void init() throws Exception {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-db.sql"), "TESTDB", "mysql");
	}

	@After
	public void after() throws Exception {
	}

	
	@Test
	public void testListFuncWithAdminAccess() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFunc.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
		List<Func> funcs = funcDao.listFunc(null, null, cra);
		assertNotNull(funcs);
		Assert.assertEquals(12, funcs.size());
		
		funcs = funcDao.listFunc("test", null, cra);
		assertNotNull(funcs);
		Assert.assertEquals(5, funcs.size());
	}

	@Test
	public void testListFuncWithLimitedAccess() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFunc.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest2'}");

		List<Func> funcs = funcDao.listFunc(null, null, cra);
		assertNotNull(funcs);
		Assert.assertEquals(4, funcs.size());
		
		funcs = funcDao.listFunc("test", null, cra);
		assertNotNull(funcs);
		Assert.assertEquals(3, funcs.size());
	}

	@Test
	public void testReadFuncWithAdminAccess() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFunc.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
		Func func = funcDao.readFunc(new FuncKey("test2", "test"),cra);
		assertNotNull(func);
	}

	@Test
	public void testReadFuncWithLimitedAccess() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFunc.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest2'}");
		Func func = funcDao.readFunc(new FuncKey("test1", "test"),cra);
		assertNotNull(func);

		func = funcDao.readFunc(new FuncKey("test2", "test"),cra);
		Assert.assertNull(func);
	}
	
	@Test
	public void testInsertUpdateAndDelete() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFunc.sql"), "TESTDB", "mysql");
		

		Func func = funcDao.readFunc(new FuncKey("test1", "test"),cra);
		assertNotNull(func);

		func.setDescription("Test Description Update");

		FuncKey funcKey =  funcDao.updateFunc(func, cra);
		func = funcDao.readFunc(new FuncKey("test1", "test"),cra);
		assertNotNull(func);
		Assert.assertEquals("Test Description Update", func.getDescription());

		funcDao.deleteFunc(funcKey, cra);
		Func readFunc = funcDao.readFunc(new FuncKey("test1", "test"),cra);
		Assert.assertNull(readFunc);
		
		
		funcKey = funcDao.insertFunc(func, cra);
		func = funcDao.readFunc(new FuncKey("test1", "test"),cra);
		assertNotNull(func);
		Assert.assertEquals("Test Description Update", func.getDescription());

	}

}
