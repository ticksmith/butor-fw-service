/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao.integration;

import static org.butor.test.ButorTestUtil.getCommonRequestArgs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.auth.common.group.Group;
import org.butor.auth.common.group.GroupItem;
import org.butor.auth.common.group.GroupItemKey;
import org.butor.auth.common.group.GroupKey;
import org.butor.auth.dao.DescDao;
import org.butor.auth.dao.GroupDao;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;

@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:test-context.xml")
public class TestSecGroup {

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}

	@Resource(name="jdbcDriverType")
	String jdbcDriverType;

	@Resource(name="secDs")
	DataSource dataSource;

	@Resource
	GroupDao groupDao;

	@Resource
	DescDao descDao;

	ButorTestUtil tu = new ButorTestUtil();
	
	@Before
	public void init() throws Exception {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-db.sql"), "TESTDB", "mysql");
	}

	@Test
	public void testReadItem() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");
		
		
		GroupItem groupItem = groupDao.readItem(new GroupItemKey("testGroup1", "testMember1", 0), cra);

		Assert.assertNotNull(groupItem);
	}
	

	@Test
	public void testUpdateGroup() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");
		
		List<GroupItem> listGroupItem = groupDao.readGroup("testGroup1", cra);

		Group group = new Group();
		group.setId("testGroup1");
		group.setIdType("group");
		
		GroupItem groupItem = new GroupItem();
		groupItem.setGroupId("testGroup1");
		groupItem.setMember("UpdateMember");
		
		listGroupItem.add(groupItem);
		listGroupItem.get(0).setMember("UpdateMember1");
		group.setItems(listGroupItem);
		
		groupDao.updateGroup(group, cra);
				
		List<GroupItem> newListGroupItem = groupDao.readGroup("testGroup1", cra);
		
		Assert.assertEquals(listGroupItem.size(), newListGroupItem.size());
	}
	
	@Test
	public void testReadGroupWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");
		
		List<GroupItem> groupItem =  groupDao.readGroup("testGroup1", cra);
		
		Assert.assertEquals(8, groupItem.size());
	}

	@Test
	public void testReadGroupWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest2'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");
		
		List<GroupItem> groupItem =  groupDao.readGroup("testGroup1", cra);
		
		Assert.assertEquals(4, groupItem.size());

		groupItem =  groupDao.readGroup("testGroup3", cra);
		
		Assert.assertEquals(0, groupItem.size());
	}

	@Test
	public void testInsertItem() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");

		GroupItem groupItem = groupDao.readItem(new GroupItemKey("testGroupInsert", "testMemberInsert", 0), cra);

		Assert.assertNull(groupItem);
		
		groupItem = new GroupItem();
		groupItem.setGroupId("testGroupInsert");
		groupItem.setMember("testMemberInsert");
		
		GroupItemKey groupItemKey = groupDao.insertItem(groupItem, cra);
		
		groupItem = groupDao.readItem(groupItemKey, cra);
		Assert.assertNotNull(groupItem);
		
	}

	@Test
	public void testDeleteItem() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");

		GroupItem groupItem = groupDao.readItem(new GroupItemKey("testGroup1", "testMember1", 0), cra);

		Assert.assertNotNull(groupItem);
		
		groupDao.deleteItem(new GroupItemKey("testGroup1", "testMember1", 0), cra);
		
		groupItem = groupDao.readItem(new GroupItemKey("testGroup1", "testMember1", 0), cra);
		Assert.assertNull(groupItem);
		
	}
	

	@Test
	public void testDeleteGroup() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");

		List<GroupItem> groupItem =  groupDao.readGroup("testGroup1", cra);
		
		Assert.assertEquals(8, groupItem.size());
		
		groupDao.deleteGroup(new GroupKey("testGroup1"), cra);
		
		groupItem =  groupDao.readGroup("testGroup1", cra);
		
		Assert.assertEquals(0, groupItem.size());
		
	}

	@Test
	public void testIsGroupRefered() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");

		boolean isGroupRefered = groupDao.isGroupRefered("testGroup1", cra);
		
		Assert.assertFalse(isGroupRefered);
		
		isGroupRefered = groupDao.isGroupRefered("sec.admin", cra);
		
		Assert.assertTrue(isGroupRefered);
		
	}


	@Test
	public void testListGroupWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");
		
		List<Group> listGroup = groupDao.listGroup("testMember1", null, cra);

		Assert.assertEquals(2, listGroup.size());

		listGroup = groupDao.listGroup(null, null, cra);

		Assert.assertEquals(4, listGroup.size());
	}

	
	@Test
	public void testListGroupWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest2'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecGroup.sql"), "TESTDB", "mysql");
		
		List<Group> listGroup = groupDao.listGroup("testMember1", null, cra);

		Assert.assertEquals(1, listGroup.size());

		listGroup = groupDao.listGroup(null, null, cra);

		Assert.assertEquals(2, listGroup.size());
	}
	

}
