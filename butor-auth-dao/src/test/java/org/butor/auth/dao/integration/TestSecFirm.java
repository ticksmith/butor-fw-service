/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao.integration;

import static org.butor.test.ButorTestUtil.getCommonRequestArgs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.auth.common.firm.Firm;
import org.butor.auth.common.firm.FirmKey;
import org.butor.auth.common.firm.FirmWithAccessMode;
import org.butor.auth.common.firm.ListFirmCriteria;
import org.butor.auth.dao.FirmDao;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.butor.utils.AccessMode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;


@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:test-context.xml")
public class TestSecFirm {

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}

	@Resource(name="secDs")
	DataSource dataSource;

	@Resource
	FirmDao firmDao;

	ButorTestUtil tu = new ButorTestUtil();
	
	@Before
	public void init() throws Exception {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-db.sql"), "TESTDB", "mysql");
	}

	
	@Test
	public void testListFirmWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFirm.sql"), "TESTDB", "mysql");
		
		ListFirmCriteria crit = new ListFirmCriteria();
		List<FirmWithAccessMode> listFirm = firmDao.listFirm(crit, cra); 
		
		Assert.assertEquals(5, listFirm.size());

		crit.setFirmName("TestFirm3");
		
		listFirm = firmDao.listFirm(crit, cra);

		Assert.assertEquals(1, listFirm.size());
	}

	@Test
	public void testListFirmWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest2'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFirm.sql"), "TESTDB", "mysql");
		
		ListFirmCriteria crit = new ListFirmCriteria();
		List<FirmWithAccessMode> listFirm = firmDao.listFirm(crit, cra); 
		
		Assert.assertEquals(3, listFirm.size());

		crit.setFirmName("TestFirm2");
		
		listFirm = firmDao.listFirm(crit, cra);

		Assert.assertEquals(0, listFirm.size());
	}

	
	
	@Test
	public void testReadFirmWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFirm.sql"), "TESTDB", "mysql");
		
		Firm firm  =  firmDao.readFirm(1, "sec", "firms", AccessMode.WRITE, cra);
		
		Assert.assertNotNull(firm);

		firm  =  firmDao.readFirm(3, "sec", "auths", AccessMode.WRITE, cra);
		
		Assert.assertNotNull(firm);
	}
	

	@Test
	public void testReadFirmWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest2'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFirm.sql"), "TESTDB", "mysql");
		
		Firm firm  =  firmDao.readFirm(1, "sec", "firms", AccessMode.WRITE, cra);
		
		Assert.assertNotNull(firm);

		firm  =  firmDao.readFirm(2, "sec", "firms", AccessMode.WRITE, cra);
		
		Assert.assertNull(firm);

		firm  =  firmDao.readFirm(1, "sec", "auths", AccessMode.WRITE, cra);
		
		Assert.assertNull(firm);
	}
	
	
	
	@Test
	public void testInsertUpdateAndDeleteFirm() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecFirm.sql"), "TESTDB", "mysql");
		

		Firm firm  =  firmDao.readFirm(1, "sec", "firms", AccessMode.WRITE, cra);
		Firm startFirm = firmDao.readFirm(1, "sec", "firms", AccessMode.WRITE, cra);
		Assert.assertEquals(firm.isActive(), true);
		Assert.assertNotNull(firm);

		
		firm.setActive(false);

		FirmKey firmKey =  firmDao.updateFirm(firm, cra);
		firm  =  firmDao.readFirm(1, "sec", "firms", AccessMode.WRITE, cra);
		Assert.assertNotNull(firm);
		Assert.assertEquals(firm.isActive(), false);

		firmDao.deleteFirm(firmKey, cra);
		firm  =  firmDao.readFirm(1, "sec", "firms", AccessMode.WRITE, cra);
		Assert.assertNull(firm);
		
		
		firmKey = firmDao.insertFirm(startFirm, cra);
		firm  =  firmDao.readFirm(1, "sec", "firms", AccessMode.WRITE, cra);
		Assert.assertNotNull(firm);
		Assert.assertEquals(firm.isActive(), true);

	}
		
}
