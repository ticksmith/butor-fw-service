--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT distinct sa.authId, sa.who, sa.whoType, sa.what, sa.whatType, sa.sys, sa.dataId,
	sa.mode, sa.startDate, sa.endDate, sa.stamp, sa.revNo, sa.userId
FROM secAuth sa
left join secUser su on
	sa.whoType = 'user' and
	su.id = sa.who
left join (__authDataSql__) dfu on
	dfu.dataType = 'firm' and
	(dfu.d1 = '*' or (dfu.d1 = su.firmId))
left join (__authDataSql__) du on
	du.dataType = 'user' and
	(du.d1 = '*' or (du.d1 = su.id))
LEFT JOIN secRole sr ON
	sa.whatType = 'role' AND
	sr.roleId = sa.what
left join (__authDataSql__) dr on
	dr.dataType = 'role' and
	(dr.d1 = '*' or dr.d1 = sr.roleId)
left join (__authDataSql__) df on
	df.dataType = 'func' and
	(df.d1 = '*' or (df.d1 = sa.sys and (df.d2 = '*' or df.d2 = sa.what)))
LEFT JOIN secGroup sg ON
	sa.whoType = 'group' AND
	sg.groupId = sa.who
left join (__authDataSql__) dg on
	dg.dataType = 'group' and
	(dg.d1 = '*' or dg.d1 = sg.groupId)
WHERE
	(sa.whoType != 'user' or dfu.d1 is not null or du.d1 is not null) and
	(sa.whoType != 'group' or dg.d1 is not null) and
	(sa.whatType != 'role' or dr.d1 is not null) and
	(sa.whatType != 'func' or df.d1 is not null) and

	(:whoTypes IS null OR ((sa.whoType IN (:whoTypes) AND
	(:whos IS null OR sa.who IN (:whos)))) OR
		(:whoTypes IN ('group') AND (sg.groupId in (:whos))) OR
		(:whoTypes IN ('user') AND (sg.member in (:whos)))) AND

	(:whatTypes IS null OR ((sa.whatType IN (:whatTypes) AND
	(:whats IS null OR sa.what IN (:whats)))) OR 
		(:whatTypes IN ('func') AND sr.func in (:whats))) AND

	(:funcSys IS null OR sa.sys = :funcSys OR
		((:whatTypes IS null OR :whatTypes IN ('func')) AND sr.sys = :funcSys)) AND

	(:withData IS null OR ((:withData=false AND sa.dataId = 0) OR
			(:withData=true AND sa.dataId != 0)))
