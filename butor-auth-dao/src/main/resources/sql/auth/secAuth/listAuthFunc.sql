--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

select func, sys, max(mode) as mode
from (
/*function->user*/
SELECT sa.what as func, sa.sys, sa.mode 
FROM secAuth sa
where 
sa.whatType = 'func' and
(sa.startDate is null OR sa.startDate <= CURRENT_TIMESTAMP) and
(sa.endDate is null OR sa.endDate >= CURRENT_TIMESTAMP) and
(sa.whoType = 'user' and sa.who = :userId)

UNION 

/*function->group->user*/
SELECT sa.what as func, sa.sys, sa.mode 
FROM secAuth sa
INNER JOIN secGroup sg ON
sg.groupId = sa.who
where 
sa.whatType = 'func' and
(sa.startDate is null OR sa.startDate <= CURRENT_TIMESTAMP) and
(sa.endDate is null OR sa.endDate >= CURRENT_TIMESTAMP) and
(sa.whoType = 'group' and sg.member = :userId)

UNION 

/*function->role->user*/
SELECT sr.func, sr.sys, sr.mode 
FROM secRole sr
INNER JOIN secAuth sa ON
sa.whatType = 'role' and
sa.what = sr.roleId
where 
(sa.startDate is null OR sa.startDate <= CURRENT_TIMESTAMP) and
(sa.endDate is null OR sa.endDate >= CURRENT_TIMESTAMP) and
(sa.whoType = 'user' and sa.who = :userId)

UNION

/*function->role->group->user*/
SELECT sr.func, sr.sys, sr.mode 
FROM secRole sr
INNER JOIN secAuth sa ON
sa.whatType = 'role' and
sa.what = sr.roleId
INNER JOIN secGroup sg ON
sg.groupId = sa.who
where 
(sa.startDate is null OR sa.startDate <= CURRENT_TIMESTAMP) and
(sa.endDate is null OR sa.endDate >= CURRENT_TIMESTAMP) and
(sa.whoType = 'group' and sg.member = :userId)
) f
group by f.sys, f.func
