--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--


/*function->user*/
SELECT sd.d1,sd.d2,sd.d3,sd.d4,sd.d5,sd.dataType,sa.mode,sa.startDate,sa.endDate
FROM secAuth sa
INNER JOIN secData sd ON
	sd.dataId = sa.dataId and
	sd.dataType IN (:dataTypes)
WHERE 
	sa.mode >= :mode and
	(sa.startDate is null OR sa.startDate <= CURRENT_TIMESTAMP) and
	(sa.endDate is null OR sa.endDate >= CURRENT_TIMESTAMP) and
	(sa.whoType = 'user' and sa.who = :member) and 
	(sa.whatType = 'func' and sa.what = :func and sa.sys = :sys)

UNION

/*function->group->user*/
SELECT sd.d1,sd.d2,sd.d3,sd.d4,sd.d5,sd.dataType,sa.mode,sa.startDate,sa.endDate
FROM secAuth sa
INNER JOIN secData sd ON
	sd.dataId = sa.dataId and
	sd.dataType IN (:dataTypes)
INNER JOIN secGroup sg ON
	sg.groupId = sa.who
WHERE 
	sa.mode >= :mode and
	(sa.startDate is null OR sa.startDate <= CURRENT_TIMESTAMP) and
	(sa.endDate is null OR sa.endDate >= CURRENT_TIMESTAMP) and
	(sa.whoType = 'group' and sg.member = :member) and 
	(sa.whatType = 'func' and sa.what = :func and sa.sys = :sys)

UNION

/*function->role->user*/
SELECT sd.d1,sd.d2,sd.d3,sd.d4,sd.d5,sd.dataType,sr.mode,sa.startDate,sa.endDate
FROM secAuth sa
INNER JOIN secData sd ON
	sd.dataId = sa.dataId and
	sd.dataType IN (:dataTypes)
INNER JOIN secRole sr ON
	sr.roleId = sa.what
WHERE 
	sr.mode >= :mode and
	(sa.startDate is null OR sa.startDate <= CURRENT_TIMESTAMP) and
	(sa.endDate is null OR sa.endDate >= CURRENT_TIMESTAMP) and
	(sa.whoType = 'user' and sa.who = :member) and 
	(sa.whatType = 'role' and sr.func = :func  and sr.sys = :sys)

UNION

/*function->role->group->user*/
SELECT sd.d1,sd.d2,sd.d3,sd.d4,sd.d5,sd.dataType,sr.mode,sa.startDate,sa.endDate
FROM secAuth sa
INNER JOIN secData sd ON
	sd.dataId = sa.dataId and
	sd.dataType IN (:dataTypes)
INNER JOIN secGroup sg ON
	sg.groupId = sa.who
INNER JOIN secRole sr ON
	sr.roleId = sa.what
WHERE 
	sr.mode >= :mode and
	(sa.startDate is null OR sa.startDate <= CURRENT_TIMESTAMP) and
	(sa.endDate is null OR sa.endDate >= CURRENT_TIMESTAMP) and
	(sa.whoType = 'group' and sg.member = :member) and 
	(sa.whatType = 'role' and sr.func = :func  and sr.sys = :sys)

