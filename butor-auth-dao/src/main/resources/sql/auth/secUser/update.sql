--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

UPDATE secUser
SET
	active = :active,
	resetInProgress = :resetInProgress,
	lastQ = :lastQ,
	missedLogin = :missedLogin,
	pwd = :pwd,
	firmId = :firmId,
	displayName = :displayName,
	fullName = :fullName,
	email = :email,
	phone = :phone,
	firstName = :firstName,
	lastName = :lastName,
	lastLoginDate = :lastLoginDate,
	attributes = :attributes,
	revNo = :revNo +1,
	stamp = CURRENT_TIMESTAMP,
	userId = :userId
WHERE
	id = :id
AND revNo = :revNo