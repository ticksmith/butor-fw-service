--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT distinct
	u.id,
	u.active,
	u.firmId,
	u.creationDate,
	u.displayName,
	u.fullName,
	u.email,
	u.phone,
	u.firstName,
	u.lastLoginDate,
	u.lastName,
	u.resetInProgress,
	u.missedLogin,
	u.attributes,
	u.revNo,
	u.stamp,
	u.userId,
	c.firmName
FROM secUser u
left join (__authDataSql__) dfu on
	dfu.dataType = 'firm' and
	(dfu.d1 = '*' or (dfu.d1 = u.firmId))
left join (__authDataSql__) du on
	du.dataType = 'user' and
	(du.d1 = '*' or (du.d1 = u.id))
LEFT JOIN secFirm c ON
	c.firmId = u.firmId	
WHERE
	(dfu.d1 is not null or du.d1 is not null) and
	(:email IS NULL OR email = :email) AND 
	(:firmId = 0 OR u.firmId = :firmId) AND 
	(:displayName IS NULL OR 
		(id like :displayName OR
		displayName like :displayName OR
		firstName like :displayName OR 
		lastName like :displayName OR
		fullName like :displayName OR
		email like :displayName)
	)
ORDER BY displayName ASC