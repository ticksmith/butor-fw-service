--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT
    u.id,
	u.q1,
	u.r1,
	u.q2,
	u.r2,
	u.q3,
	u.r3,
	u.avatar,
	u.lastQ,
	u.revNo,
	u.stamp,
	u.userId
FROM
	secUser u
WHERE
	u.id = :id
