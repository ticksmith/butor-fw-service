--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT dataId,sys,dataType,d1,d2,d3,d4,d5,stamp,revNo,userId
FROM secData sd
WHERE 
(:dataId is null or dataId = :dataId) and
(:sys is null or sys = :sys) and
(:dataType is null or dataType = :dataType) and
(:d1 is null or d1 = :d1) and
(:d2 is null or d2 = :d2) and
(:d3 is null or d3 = :d3) and
(:d4 is null or d4 = :d4) and
(:d5 is null or d5 = :d5)
ORDER BY dataType
