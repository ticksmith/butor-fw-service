--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT distinct
	r.groupId,
	r.member,
	r.stamp,
	r.revNo,
	r.userId
FROM secGroup r
inner join (__authDataSql__) dg on
	dg.dataType = 'group' and
	(dg.d1 = '*' or dg.d1 = r.groupId)
left join secUser su on
	su.id = r.member
left join (__authDataSql__) dfu on
	dfu.dataType = 'firm' and
	(dfu.d1 = '*' or (dfu.d1 = su.firmId))
left join (__authDataSql__) du on
	du.dataType = 'user' and
	(du.d1 = '*' or (du.d1 = r.member))
where 
	r.groupId = :groupId and
	(dfu.d1 is not null or du.d1 is not null)

order by r.member 
