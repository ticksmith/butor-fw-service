--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT distinct
	sf.active,
	sf.theme,
	sf.firmId,
	sf.firmName,
	sf.contactName,
	sf.contactPhone,
	sf.creationDate,
	sf.attributes,
	sf.revNo,
	sf.stamp,
	sf.userId,
	max(d.mode) mode
FROM
	${security.dbName:PORTAL}.secFirm sf
inner join (__authDataSql__) d on
	d.dataType = 'firm' and
	(d.d1 = '*' or d.d1 = sf.firmId)
WHERE
	(:firmName IS NULL OR firmName like :firmName)
GROUP BY firmId
ORDER BY firmName ASC