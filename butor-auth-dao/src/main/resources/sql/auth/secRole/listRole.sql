--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT distinct
	sd.id,
	sd.description,
	sd.stamp,
	sd.userId,
	sd.revNo
FROM secDesc sd
inner join (__authDataSql__) dr on
	dr.dataType = 'role' and
	(dr.d1 = '*' or dr.d1 = sd.id)
left join secRole sr on
	sd.id = sr.roleId
where sd.idType = 'role'
and (:funcId IS NULL OR sr.func = :funcId)
and (:funcSys IS NULL OR sr.sys = :funcSys)
