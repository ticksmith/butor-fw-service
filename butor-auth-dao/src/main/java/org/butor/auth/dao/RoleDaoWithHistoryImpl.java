/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Iterator;
import java.util.List;

import org.butor.auth.common.role.Role;
import org.butor.auth.common.role.RoleItem;
import org.butor.auth.common.role.RoleItemKey;
import org.butor.auth.common.role.RoleKey;
import org.butor.dao.DaoWithHistory;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class RoleDaoWithHistoryImpl extends org.butor.auth.dao.RoleDaoImpl implements DaoWithHistory {
	private ThreadLocal<List<RoleItem>> dataTL = new ThreadLocal<List<RoleItem>>();
	@Override
	public void deleteRole(RoleKey roleKey, CommonRequestArgs cra) {
		try {
			List<RoleItem> oldFuncs = readRole(roleKey.getId(), cra);
			dataTL.set(oldFuncs);
			for (RoleItem ori : oldFuncs) {
				// func removed
				RoleItemKey rik = new RoleItemKey(roleKey.getId(), ori.getFunc(), ori.getSys(), ori.getRevNo());
				deleteItem(rik, cra);
			}
		} finally {
			dataTL.remove();
		}
	}
	@Override
	public void updateRole(Role role, CommonRequestArgs cra) {
		try {
			List<RoleItem> funcs = role.getItems();
			List<RoleItem> oldFuncs = readRole(role.getId(), cra);
			dataTL.set(oldFuncs);
			Iterator<RoleItem> it = oldFuncs.iterator();
			while (it.hasNext()) {
				boolean funcRemoved = true;
				RoleItem ori = it.next();
				for (RoleItem ri : funcs) {
					if (ri.getFunc().equalsIgnoreCase(ori.getFunc()) && ri.getSys().equalsIgnoreCase(ori.getSys())) {
						funcRemoved = false;
						break; // role still exists
					}
				}
				if (funcRemoved) {
					RoleItemKey rik = new RoleItemKey(role.getId(), ori.getFunc(), ori.getSys(), ori.getRevNo());
					deleteItem(rik, cra);
					it.remove();
				}
			}
			for (RoleItem ri : funcs) {
				boolean funcAdded = true;
				boolean modeChanged = false;
				for (RoleItem ori : oldFuncs) {
					if (ri.getFunc().equalsIgnoreCase(ori.getFunc()) && ri.getSys().equalsIgnoreCase(ori.getSys())) {
						if (ri.getMode() != ori.getMode()) {
							modeChanged = true;
						}
						funcAdded = false;
						break; // role still exists
					}
				}
				if (funcAdded) {
					ri.setRoleId(role.getId());
					RoleItemKey uk = insertItem(ri, cra);
					if (uk == null) {
						ApplicationException.exception(
								CommonMessageID.SERVICE_FAILURE.getMessage());
						return;
					}
				} else if (modeChanged) {
					ri.setRoleId(role.getId());
					RoleItemKey rik = new RoleItemKey(ri.getRoleId(), ri.getFunc(), ri.getSys(), ri.getRevNo());
					deleteItem(rik, cra);
					RoleItemKey uk = insertItem(ri, cra);
					if (uk == null) {
						ApplicationException.exception(
								CommonMessageID.SERVICE_FAILURE.getMessage());
						return;
					}
				}
			}
		} finally {
			dataTL.remove();
		}
	}
	@Override
	public Object getRowForHistory(MapSqlParameterSource params) {
		List<RoleItem> data = dataTL.get();
		if (data == null || data.size() == 0) {
			return null;
		}

		String func = (String)params.getValue("func");
		String sys = (String)params.getValue("sys");
		for (RoleItem item : data) {
			if (item.getFunc().equals(func) && item.getSys().equals(sys)) {
				return item;
			}
		}
		return null;
	}
	@Override
	public String getInsertSql() {
		return super.getInsertItemSql();
	}
}
