/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.butor.auth.common.AuthDataFilter;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.func.Func;
import org.butor.auth.common.func.FuncKey;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.ArgsBuilder;

import com.google.api.client.util.Strings;

public class FuncDaoImpl extends AbstractDao implements FuncDao {

	private final String PROC_LIST_FUNC = getClass().getName() +".listFunc";
	private final String PROC_READ_FUNC = getClass().getName() +".readFunc";
	private final String PROC_INSERT_FUNC = getClass().getName() +".insertFunc";
	private final String PROC_UPDATE_FUNC = getClass().getName() +".updateFunc";
	private final String PROC_DELETE_FUNC = getClass().getName() +".deleteFunc";

	private String listSql;
	private String readSql;
	private String deleteSql;
	private String updateSql;
	private String insertSql;

	private String systemId;
	private String secFunc;
	private AuthDao authDao;

	@Override
	public List<Func> listFunc(String sys, String func, CommonRequestArgs cra) {
		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(SecurityConstants.SYSTEM_ID);
		authData.setFunc(Strings.isNullOrEmpty(func) ? SecurityConstants.SEC_FUNC_FUNCS : func);
		authData.setDataTypes("func");
		authData.setMode((AccessMode.READ).value());
		authData.setMember(cra.getUserId());

		Map<String, Object> args = ArgsBuilder.create().set("funcSys", sys).build();

		return queryList(PROC_LIST_FUNC, this.listSql, Func.class, args, authData, cra);
	}

	public void setListSql(String listSql_) {
		this.listSql = listSql_;
	}

	public void setReadSql(String readSql) {
		this.readSql = readSql;
	}

	@Override
	public Func readFunc(FuncKey funcKey, CommonRequestArgs cra) {
		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(SecurityConstants.SYSTEM_ID);
		authData.setFunc(SecurityConstants.SEC_FUNC_FUNCS);
		authData.setDataTypes("func");
		authData.setMode((AccessMode.READ).value());
		authData.setMember(cra.getUserId());

		Map<String, Object> args = ArgsBuilder.create().set("funcSys", funcKey.getSys()).set("funcId", funcKey.getFunc()).build();
		
		List<Func> list = queryList(PROC_READ_FUNC, this.readSql, Func.class, authData, cra, args);
		if (list == null || list.size() == 0)
			return null;
		return list.get(0);
	}

	@Override
	public FuncKey insertFunc(Func func, CommonRequestArgs cra) {
		if (!authDao.hasAccess(systemId, secFunc, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}

		func.setStamp(new Date());
		func.setRevNo(0);

		UpdateResult ur = insert(PROC_INSERT_FUNC, this.insertSql, func, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.INSERT_FAILURE.getMessage());
		}
		return new FuncKey(func.getFunc(), func.getSys(), 0);
	}

	@Override
	public FuncKey updateFunc(Func func, CommonRequestArgs cra) {
		if (!authDao.hasAccess(systemId, secFunc, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}

		UpdateResult ur = update(PROC_UPDATE_FUNC, this.updateSql, func, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new FuncKey(func.getFunc(), func.getSys(), func.getRevNo()+1);
	}

	@Override
	public void deleteFunc(FuncKey funcKey, CommonRequestArgs cra) {
		if (!authDao.hasAccess(systemId, secFunc, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		UpdateResult ur = update(PROC_DELETE_FUNC, this.deleteSql, funcKey, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}

	public void setUpdateSql(String updateSql) {
		this.updateSql = updateSql;
	}

	public void setInsertSql(String insertSql) {
		this.insertSql = insertSql;
	}

	public void setDeleteSql(String deleteSql) {
		this.deleteSql = deleteSql;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public void setSecFunc(String secFunc) {
		this.secFunc = secFunc;
	}

	public void setAuthDao(AuthDao authDao) {
		this.authDao = authDao;
	}
}