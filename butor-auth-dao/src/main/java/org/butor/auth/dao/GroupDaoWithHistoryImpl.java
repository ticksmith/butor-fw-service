/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Iterator;
import java.util.List;

import org.butor.auth.common.group.Group;
import org.butor.auth.common.group.GroupItem;
import org.butor.auth.common.group.GroupItemKey;
import org.butor.auth.common.group.GroupKey;
import org.butor.dao.DaoWithHistory;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class GroupDaoWithHistoryImpl extends org.butor.auth.dao.GroupDaoImpl implements DaoWithHistory {
	private ThreadLocal<List<GroupItem>> dataTL = new ThreadLocal<List<GroupItem>>();
	@Override
	public void deleteGroup(GroupKey groupKey, CommonRequestArgs cra) {
		try {
			List<GroupItem> oldMembers = readGroup(groupKey.getId(), cra);
			dataTL.set(oldMembers);
			for (GroupItem ogi : oldMembers) {
				// member removed
				GroupItemKey gik = new GroupItemKey(groupKey.getId(), ogi.getMember(), ogi.getRevNo());
				deleteItem(gik, cra);
			}
		} finally {
			dataTL.remove();
		}
	}
	@Override
	public void updateGroup(Group group, CommonRequestArgs cra) {
		try {
			List<GroupItem> members = group.getItems();
			group.setIdType("group");
	
			List<GroupItem> oldMembers = readGroup(group.getId(), cra);
			dataTL.set(oldMembers);
			Iterator<GroupItem> it = oldMembers.iterator();
			while (it.hasNext()) {
				boolean memberRemoved = true;
				GroupItem ogi = it.next();
				for (GroupItem gi : members) {
					if (gi.getMember().equalsIgnoreCase(ogi.getMember())) {
						memberRemoved = false;
						continue; // member still exists
					}
				}
				if (memberRemoved) {
					GroupItemKey gik = new GroupItemKey(group.getId(), ogi.getMember(), ogi.getRevNo());
					deleteItem(gik, cra);
					it.remove();
				}
			}
			for (GroupItem gi : members) {
				boolean memberAdded = true;
				for (GroupItem ogi : oldMembers) {
					if (gi.getMember().equalsIgnoreCase(ogi.getMember())) {
						memberAdded = false;
						continue; // member still exists
					}
				}
				if (memberAdded) {
					gi.setGroupId(group.getId());
					GroupItemKey uk = insertItem(gi, cra);
					if (uk == null) {
						ApplicationException.exception(
								CommonMessageID.SERVICE_FAILURE.getMessage());
						return;
					}
				}
			}
		} finally {
			dataTL.remove();
		}
	}
	@Override
	public Object getRowForHistory(MapSqlParameterSource params) {
		List<GroupItem> data = dataTL.get();
		if (data == null || data.size() == 0) {
			return null;
		}
		String member = (String)params.getValue("member");
		for (GroupItem item : data) {
			if (item.getMember().equals(member)) {
				return item;
			}
		}
		return null;
	}
	@Override
	public String getInsertSql() {
		return super.getInsertItemSql();
	}
}
