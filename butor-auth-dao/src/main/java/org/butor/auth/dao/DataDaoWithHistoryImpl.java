/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Iterator;
import java.util.List;

import org.butor.auth.common.auth.SecData;
import org.butor.dao.DaoWithHistory;
import org.butor.json.CommonRequestArgs;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class DataDaoWithHistoryImpl extends org.butor.auth.dao.DataDaoImpl implements DaoWithHistory {
	private ThreadLocal<List<SecData>> dataTL = new ThreadLocal<List<SecData>>();
	@Override
	public void deleteData(long dataId, CommonRequestArgs cra) {
		try {
			SecData oldSecData = new SecData();
			oldSecData.setDataId(dataId);
			List<SecData> oldData = listData(oldSecData, cra);
			dataTL.set(oldData);
			for (SecData sd : oldData) {
				deleteItem(sd, cra);
			}
		} finally {
			dataTL.remove();
		}
	}
	@Override
	public long updateData(long dataId, List<SecData> data, CommonRequestArgs cra) {
		try {
			List<SecData> oldData = null;
			if (dataId > -1) {
				SecData oldSecData = new SecData();
				oldSecData.setDataId(dataId);
				oldData = listData(oldSecData, cra);
				dataTL.set(oldData);
				if (oldData != null) {
					Iterator<SecData> it = oldData.iterator();
					while (it.hasNext()) {
						boolean sdRemoved = true;
						SecData osd = it.next();
						if (data != null) {
							for (SecData sd : data) {
								String k1 = buildSecDataKey(sd);
								String k2 = buildSecDataKey(osd);
								if (k1.equals(k2)) {
									sdRemoved = false;
									continue; // member still exists
								}
							}
						}
						if (sdRemoved) {
							deleteItem(osd, cra);
							it.remove();
						}
					}
				}
			}
			// all occurrences of data must have the same dataId.
			// get the first generated one and set it to others
			if (data != null) {
				for (SecData sd : data) {
					boolean sdAdded = true;
					sd.setUserId(cra.getUserId());
					if (oldData != null) {
						for (SecData osd : oldData) {
							if (buildSecDataKey(sd).equals(buildSecDataKey(osd))) {
								sdAdded = false;
								continue; // member still exists
							}
						}
					}
					if (sdAdded) {
						validateData(sd);
						if (dataId > -1) {
							sd.setDataId(dataId);
						}
						dataId = insertData(sd, cra);
					}
				}
			}
		} finally {
			dataTL.remove();
		}
		
		return dataId;
	}
	@Override
	public Object getRowForHistory(MapSqlParameterSource params) {
		List<SecData> list = dataTL.get();
		if (list == null || list.size() == 0) {
			return null;
		}

		SecData sd = new SecData();
		sd.setDataType((String)params.getValue("dataType"));
		sd.setSys((String)params.getValue("sys"));
		sd.setRevNo((Integer)params.getValue("revNo"));
		sd.setD1((String)params.getValue("d1"));
		sd.setD2((String)params.getValue("d2"));
		sd.setD3((String)params.getValue("d3"));
		sd.setD4((String)params.getValue("d4"));
		sd.setD5((String)params.getValue("d5"));

		for (SecData item : list) {
			if (buildSecDataKey(item).equals(buildSecDataKey(sd))) {
				return item;
			}
		}
		return null;
	}
	@Override
	public String getInsertSql() {
		return super.getInsertDataSql();
	}
}
