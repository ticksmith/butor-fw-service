/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Strings;

import org.butor.auth.common.AuthDataFilter;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.user.ListUserCriteria;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserKey;
import org.butor.auth.common.user.UserQuestions;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.dao.DaoWithHistory;
import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonHelper;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.util.StringUtils;

public class UserDaoImpl extends AbstractDao implements UserDao, DaoWithHistory {

	private String listSql;
	private String readSql;
	private String readQuestionsSql;
	private String deleteSql;
	private String updateSql;
	private String updateQuestionsSql;
	private String updateStateSql;
	private String insertSql;

	private AuthDao authDao;

	private final String PROC_LIST_USER = getClass().getName() +".listUser";
	private final String PROC_READ_USER = getClass().getName() +".readUser";
	private final String PROC_READ_QUESTIONS = getClass().getName() +".readQuestions";
	private final String PROC_INSERT_USER = getClass().getName() +".insertUser";
	private final String PROC_UPDATE_USER = getClass().getName() +".updateUser";
	private final String PROC_UPDATE_STATE = getClass().getName() +".updateState";
	private final String PROC_UPDATE_QUESTIONS = getClass().getName() +".updateQuestions";
	private final String PROC_DELETE_USER = getClass().getName() +".deleteUser";

	@Override
	public List<User> listUser(ListUserCriteria crit, String func, CommonRequestArgs cra) {

		if (StringUtils.hasLength(crit.getDisplayName())) {
			crit.setDisplayName("%" + crit.getDisplayName() + "%");
		}

		AuthDataFilter authDataCrit = new AuthDataFilter();
		authDataCrit.setSys(SecurityConstants.SYSTEM_ID);
		authDataCrit.setFunc(Strings.isNullOrEmpty(func) ? SecurityConstants.SEC_FUNC_USERS : func);
		authDataCrit.setDataTypes(SecurityConstants.SEC_DATA_TYPE_USER, SecurityConstants.SEC_DATA_TYPE_FIRM);
		authDataCrit.setMode((AccessMode.READ).value());
		authDataCrit.setMember(cra.getUserId());
		List<User> userList = queryList(PROC_LIST_USER, listSql, User.class, crit, cra, authDataCrit);
		JsonHelper jsh = new JsonHelper();
		for (User u : userList) {
			if (u.getAttributes() != null) {
				u.setAttributesMap(jsh.deserialize(u.getAttributes(), Map.class));
			}
		}
		return userList;
	}

	public void setListSql(String listSql_) {
		this.listSql = listSql_;
	}

	public void setReadSql(String readSql) {
		this.readSql = readSql;
	}

	@Override
	public User readUser(String id, String func, CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", id);
		
		AuthDataFilter authDataCrit = new AuthDataFilter();
		authDataCrit.setSys(SecurityConstants.SYSTEM_ID);
		authDataCrit.setFunc(Strings.isNullOrEmpty(func) ? SecurityConstants.SEC_FUNC_USERS : func);
		authDataCrit.setDataTypes(SecurityConstants.SEC_DATA_TYPE_USER, SecurityConstants.SEC_DATA_TYPE_FIRM);
		authDataCrit.setMode((AccessMode.READ).value());
		authDataCrit.setMember(cra.getUserId());
		
		User user = queryFirst(PROC_READ_USER, readSql, User.class, args, cra, authDataCrit);
		if (user != null && !Strings.isNullOrEmpty(user.getAttributes())) {
			JsonHelper jsh = new JsonHelper();
			user.setAttributesMap(jsh.deserialize(user.getAttributes(), Map.class));
		}
		return user;
	}

	@Override
	public UserQuestions readQuestions(String id, CommonRequestArgs cra) {
		preventOtherUsersAccess(cra, AccessMode.READ,id);
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", id);
		return queryFirst(PROC_READ_QUESTIONS, readQuestionsSql, UserQuestions.class, args, cra);
	}

	@Override
	public UserKey insertUser(User user, CommonRequestArgs cra) {
		if (!authDao.hasAccess("sec", "users", AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		user.setStamp(new Date());
		user.setCreationDate(user.getStamp());
		user.setRevNo(0);
		if (user.getAttributesMap() != null) {
			JsonHelper jsh = new JsonHelper();
			user.setAttributes(jsh.serialize(user.getAttributesMap()));
		}
		UpdateResult ur = insert(PROC_INSERT_USER, insertSql, user, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new UserKey(user.getId(), user.getRevNo());
	}

	@Override
	public UserKey updateState(User user, CommonRequestArgs cra) {
		preventOtherUsersAccess(cra, AccessMode.WRITE,user.getId());
		UpdateResult ur = update(PROC_UPDATE_STATE, updateStateSql, user, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new UserKey(user.getId(), user.getRevNo() +1);
	}

	@Override
	public UserKey updateUser(User user, CommonRequestArgs cra) {
		if (!authDao.hasAccess("sec", "users", AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		if (user.getAttributesMap() != null) {
			JsonHelper jsh = new JsonHelper();
			user.setAttributes(jsh.serialize(user.getAttributesMap()));
		}
		UpdateResult ur = update(PROC_UPDATE_USER, updateSql, user, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new UserKey(user.getId(), user.getRevNo() +1);
	}

	@Override
	public UserKey updateQuestions(UserQuestions user, CommonRequestArgs cra) {
		preventOtherUsersAccess(cra, AccessMode.WRITE,user.getId());
		UpdateResult ur = update(PROC_UPDATE_QUESTIONS, updateQuestionsSql, user, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new UserKey(user.getId(), user.getRevNo() +1);
	}

	@Override
	public void deleteUser(UserKey userKey, CommonRequestArgs cra) {
		if (!authDao.hasAccess("sec", "users", AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		UpdateResult ur = delete(PROC_DELETE_USER, deleteSql, userKey, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}
	@Override
	public Object getRowForHistory(MapSqlParameterSource params) {
		CommonRequestArgs cra = new CommonRequestArgs();
		cra.setUserId((String)params.getValue("userId"));
		cra.setLang((String)params.getValue("lang"));
		cra.setReqId((String)params.getValue("reqId"));
		cra.setSessionId((String)params.getValue("sessionId"));
		String id = (String)params.getValue("id");

		return readUser(id, null, cra);
	}
	
	/**
	 * This method prevent a user to forge parameters that could make possible the access
	 * to other's user data when a service is invoked.
	 * 
	 *   Used in the case of self-serve service (question/state)
	 */
	
	protected void preventOtherUsersAccess(CommonRequestArgs cra,AccessMode mode, String id) {
		if (!authDao.hasAccess("sec", "users", mode, cra)) {
			// If you ARE NOT an admin, and you try to do an access to a user id that is not yours
			// then you're not AUTHORIZED
			if (cra.getUserId() == null ||  !cra.getUserId().equals(id)) {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}
		}
	}
	
	@Override
	public String getInsertSql() {
		return insertSql;
	}

	public void setUpdateSql(String updateSql) {
		this.updateSql = updateSql;
	}

	public void setInsertSql(String insertSql) {
		this.insertSql = insertSql;
	}

	public void setDeleteSql(String deleteSql) {
		this.deleteSql = deleteSql;
	}

	public void setReadQuestionsSql(String readQuestionsSql) {
		this.readQuestionsSql = readQuestionsSql;
	}

	public void setUpdateQuestionsSql(String updateQuestionsSql) {
		this.updateQuestionsSql = updateQuestionsSql;
	}

	public void setUpdateStateSql(String updateStateSql) {
		this.updateStateSql = updateStateSql;
	}

	public void setAuthDao(AuthDao authDao) {
		this.authDao = authDao;
	}
}