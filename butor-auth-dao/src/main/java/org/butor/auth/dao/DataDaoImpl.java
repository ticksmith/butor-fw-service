/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.butor.auth.common.AuthMessageID;
import org.butor.auth.common.auth.SecData;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.ApplicationException;

import com.google.common.base.Strings;


public class DataDaoImpl extends AbstractDao implements DataDao {

	private String listDataSql;
	private String readItemSql;
	private String deleteDataSql;
	private String deleteItemSql;
	private String insertDataSql;

	protected final String PROC_LIST_DATA = getClass().getName() +".listData";
	protected final String PROC_READ_DATA = getClass().getName() +".readData";
	protected final String PROC_DELETE_DATA = getClass().getName() +".deleteData";
	protected final String PROC_DELETE_ITEM = getClass().getName() +".deleteItem";
	protected final String PROC_INSERT_DATA = getClass().getName() +".insertData";

	@Override
	public List<SecData> listData(SecData data, CommonRequestArgs cra) {
		return queryList(PROC_LIST_DATA, this.listDataSql, SecData.class, data, cra);
	}
	@Override
	public SecData readItem(SecData data, CommonRequestArgs cra) {
		return queryFirst(PROC_READ_DATA, this.readItemSql, SecData.class, data, cra);
	}

	@Override
	public void deleteData(long dataId, CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("dataId", dataId);
		UpdateResult ur = delete(PROC_DELETE_DATA, deleteDataSql, args, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}
	@Override
	public long updateData(long dataId, List<SecData> data, CommonRequestArgs cra) {
		List<SecData> oldData = null;
		SecData toUpdateData = new SecData();
		if (dataId > -1) {
			toUpdateData.setDataId(dataId);
			oldData = listData(toUpdateData, cra);
			if (oldData != null) {
				Iterator<SecData> it = oldData.iterator();
				while (it.hasNext()) {
					boolean sdRemoved = true;
					SecData osd = it.next();
					if (data != null) {
						for (SecData sd : data) {
							String k1 = buildSecDataKey(sd);
							String k2 = buildSecDataKey(osd);
							if (k1.equals(k2)) {
								sdRemoved = false;
								continue; // member still exists
							}
						}
					}
					if (sdRemoved) {
						deleteItem(osd, cra);
						it.remove();
					}
				}
			}
		}
		// all occurrences of data must have the same dataId.
		// get the first generated one and set it to others
		if (data != null) {
			for (SecData sd : data) {
				boolean sdAdded = true;
				sd.setUserId(cra.getUserId());
				if (oldData != null) {
					for (SecData osd : oldData) {
						if (buildSecDataKey(sd).equals(buildSecDataKey(osd))) {
							sdAdded = false;
							continue; // member still exists
						}
					}
				}
				if (sdAdded) {
					validateData(sd);
					if (dataId > -1) {
						sd.setDataId(dataId);
					}
					dataId = insertData(sd, cra);
				}
			}
		}
		return dataId;
	}
	@Override
	public void deleteItem(SecData data, CommonRequestArgs cra) {
		UpdateResult ur = delete(PROC_DELETE_ITEM, deleteItemSql, data, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}

	@Override
	public long insertData(SecData data, CommonRequestArgs cra) {
		data.setStamp(new Date());
		data.setRevNo(0);
		UpdateResult ur = insert(PROC_INSERT_DATA, insertDataSql, data, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.INSERT_FAILURE.getMessage());
		}
		return ur.key;
	}

	public void setListDataSql(String listDataSql) {
		this.listDataSql = listDataSql;
	}

	public void setDeleteDataSql(String deleteDataSql) {
		this.deleteDataSql = deleteDataSql;
	}

	public void setInsertDataSql(String insertDataSql) {
		this.insertDataSql = insertDataSql;
	}
	public String getInsertDataSql() {
		return insertDataSql;
	}
	public void setDeleteItemSql(String deleteItemSql) {
		this.deleteItemSql = deleteItemSql;
	}
	
	public void setReadItemSql(String readItemSql) {
		this.readItemSql = readItemSql;
	}
	public String getListDataSql() {
		return listDataSql;
	}
	protected String buildSecDataKey(SecData sd) {
		return sd.getDataType() +"." +
				Strings.nullToEmpty(sd.getD1()) +"." +
				Strings.nullToEmpty(sd.getD2()) +"." +
				Strings.nullToEmpty(sd.getD3()) +"." +
				Strings.nullToEmpty(sd.getD4()) +"." +
				Strings.nullToEmpty(sd.getD5());
	}
	public void validateData(SecData sd) throws ApplicationException {
		if (Strings.isNullOrEmpty(sd.getDataType())) {
			ApplicationException.exception(AuthMessageID.MISSING_DATA_TYPE.getMessage());
		}
		if (Strings.isNullOrEmpty(sd.getSys())) {
			ApplicationException.exception(AuthMessageID.MISSING_SYS.getMessage());
		}
		if (Strings.isNullOrEmpty(sd.getD1()) &&
				Strings.isNullOrEmpty(sd.getD2()) &&
				Strings.isNullOrEmpty(sd.getD3()) &&
				Strings.isNullOrEmpty(sd.getD4()) &&
				Strings.isNullOrEmpty(sd.getD5())) {
			ApplicationException.exception(AuthMessageID.MISSING_DATA_FIELD.getMessage());
		}
		if (sd.getD1() == null) {
			sd.setD1("");
		}
		if (sd.getD2() == null) {
			sd.setD2("");
		}
		if (sd.getD3() == null) {
			sd.setD3("");
		}
		if (sd.getD4() == null) {
			sd.setD4("");
		}
		if (sd.getD5() == null) {
			sd.setD5("");
		}
	}
}