/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.List;

import org.butor.auth.common.func.Func;
import org.butor.auth.common.func.FuncKey;
import org.butor.json.CommonRequestArgs;

public interface FuncDao {
	Func readFunc(FuncKey funcKey, CommonRequestArgs cra);
	FuncKey insertFunc(Func func, CommonRequestArgs cra);
	void deleteFunc(FuncKey funcKey, CommonRequestArgs cra);
	FuncKey updateFunc(Func func, CommonRequestArgs cra);
	List<Func> listFunc(String sys, String func, CommonRequestArgs cra);
}
