/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.butor.auth.common.AuthDataFilter;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.audit.Audit;
import org.butor.auth.common.audit.AuditCriteria;
import org.butor.auth.common.audit.ServiceCall;
import org.butor.auth.common.audit.ServiceCallCriteria;
import org.butor.dao.AbstractDao;
import org.butor.dao.RowHandler;
import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonHelper;
import org.butor.utils.AccessMode;
import org.butor.utils.ArgsBuilder;

import com.google.api.client.util.Strings;
import com.google.common.collect.Maps;

public class AuditDaoImpl extends AbstractDao implements AuditDao {

	private String listAuditSql;
	private String listServiceCallSql;
	private static final long LIMIT = 1000;
	
	// fields of user attributes to show in audit
	// use setter to change/override this list
	private List<String> auditUserAttributes = Arrays.asList("ip", "userAgent", "reverseLookup", "domain");

	private JsonHelper jsonHelper = new JsonHelper();
	private final String PROC_LIST_AUDIT_FUNC = getClass().getName() +".listAudit";
	@Override
	public List<Audit> listAudit(AuditCriteria criteria, CommonRequestArgs commonRequestArgs) {
		AuthDataFilter authDataCrit = getAuthDataFilterForServiceCalls(commonRequestArgs);
		List<Audit> al = queryList(PROC_LIST_AUDIT_FUNC, this.listAuditSql, Audit.class, criteria, commonRequestArgs,authDataCrit);

		// return to audit only allowed user attributes
		for (Audit ar : al) {
			if (!Strings.isNullOrEmpty(ar.getAttributes())) {
				Map<String,Object> allAtrs = jsonHelper.deserialize(ar.getAttributes(), Map.class);
				Map<String,Object> auditAtrs = Maps.newHashMap();
				if (auditUserAttributes != null && allAtrs != null) {
					for (String attr : auditUserAttributes) {
						auditAtrs.put(attr, allAtrs.get(attr));
					}
				}
				ar.setAttributesMap(auditAtrs);
			}
		}
		return al;
	}

	@Override
	public List<ServiceCall> listServiceCalls(ServiceCallCriteria criteria,
			CommonRequestArgs commonRequestArgs) {
		AuthDataFilter authDataCrit = getAuthDataFilterForServiceCalls(commonRequestArgs);
		ArgsBuilder params  = ArgsBuilder.create();
		params.addAllFieldsFromObject(criteria);
		params.set("limit", LIMIT);
		List<ServiceCall> al = queryList(PROC_LIST_AUDIT_FUNC, this.listServiceCallSql, ServiceCall.class, params.build(), commonRequestArgs,authDataCrit);
		return al;
	}

	
	@Override
	public void exportServiceCalls(ServiceCallCriteria criteria,
			CommonRequestArgs commonRequestArgs, RowHandler<ServiceCall> rowHandler) {
		AuthDataFilter authDataCrit = getAuthDataFilterForServiceCalls(commonRequestArgs);
		ArgsBuilder params  = ArgsBuilder.create();
		params.addAllFieldsFromObject(criteria);
		params.set("limit", Integer.MAX_VALUE);
		queryList(PROC_LIST_AUDIT_FUNC, this.listServiceCallSql, ServiceCall.class, rowHandler, params.build(), commonRequestArgs, authDataCrit);
	}

	private AuthDataFilter getAuthDataFilterForServiceCalls(
			CommonRequestArgs commonRequestArgs) {
		AuthDataFilter authDataCrit = new AuthDataFilter();
		authDataCrit.setSys(SecurityConstants.SYSTEM_ID);
		authDataCrit.setFunc(SecurityConstants.SEC_FUNC_AUDIT_SERVICECALL);
		authDataCrit.setDataTypes(SecurityConstants.SEC_DATA_TYPE_USER, SecurityConstants.SEC_DATA_TYPE_FIRM);
		authDataCrit.setMode((AccessMode.READ).value());
		authDataCrit.setMember(commonRequestArgs.getUserId());
		return authDataCrit;
	}

	
	public void setListAuditSql(String listAuditSql) {
		this.listAuditSql = listAuditSql;
	}

	public void setAuditUserAttributes(List<String> auditUserAttributes) {
		this.auditUserAttributes = auditUserAttributes;
	}

	public void setListServiceCallSql(String listServiceCallSql) {
		this.listServiceCallSql = listServiceCallSql;
	}

}
