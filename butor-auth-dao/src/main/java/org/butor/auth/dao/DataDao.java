/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.List;

import org.butor.auth.common.auth.SecData;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.ApplicationException;


public interface DataDao {
	SecData readItem(SecData data, CommonRequestArgs cra);
	List<SecData> listData(SecData data, CommonRequestArgs cra);
	void deleteData(long dataId, CommonRequestArgs cra);
	long updateData(long dataId, List<SecData> data, CommonRequestArgs cra);
	void deleteItem(SecData data, CommonRequestArgs cra);
	long insertData(SecData data, CommonRequestArgs cra);
	void validateData(SecData sd) throws ApplicationException;
}
