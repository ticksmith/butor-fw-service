/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.butor.auth.common.AuthData;
import org.butor.auth.common.ListAuthDataCriteria;
import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.auth.UserAuthFunc;
import org.butor.auth.common.auth.AuthKey;
import org.butor.auth.common.auth.ListAuthCriteria;
import org.butor.auth.common.auth.ListUserAuthFuncCriteria;
import org.butor.auth.common.func.Func;
import org.butor.dao.AbstractDao;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.AccessMode;

public class AuthDaoDummyImpl extends AbstractDao implements AuthDao {

	@Override
	public List<Func> listAuthFunc(CommonRequestArgs cra) {
		List<Func> el = Collections.emptyList();
		return el;
	}

	@Override
	public List<AuthData> listAuthData(ListAuthDataCriteria args, CommonRequestArgs cra) {
		List<AuthData> el = Collections.emptyList();
		return el;
	}

	@Override
	public List<Auth> listAuth(ListAuthCriteria args, CommonRequestArgs cra) {
		List<Auth> el = Collections.emptyList();
		return el; 
	}

	@Override
	public List<UserAuthFunc> listUserAuthFunc(ListUserAuthFuncCriteria args, CommonRequestArgs cra) {
		List<UserAuthFunc> el = Collections.emptyList();
		return el;
	}

	@Override
	public Map<String, List<String>> prepareAuthData(String system, String func, AccessMode am, CommonRequestArgs cra,
			String... dataTypes) {
		Map<String, List<String>> em = new HashMap<String, List<String>>();
		for (String dt : dataTypes) {
			List<String> list = em.get(dt);
			if (list == null) {
				list = new ArrayList<String>();
				em.put(dt +"_list", list);
			}
			list.add("ALL");
		}
		return em;
	}


	@Override
	public boolean hasAccess(String system, String func, AccessMode am, CommonRequestArgs cra) {
		return true;
	}

	@Override
	public Auth readAuth(int authId, CommonRequestArgs cra) {
		return null;
	}

	@Override
	public AuthKey updateAuth(Auth auth, CommonRequestArgs cra) {
		return null;
	}

	@Override
	public AuthKey insertAuth(Auth auth, CommonRequestArgs cra) {
		return null;
	}

	@Override
	public void deleteAuth(AuthKey ak, CommonRequestArgs cra) {
	}

}