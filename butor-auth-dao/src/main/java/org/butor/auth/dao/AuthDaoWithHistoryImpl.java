/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import org.butor.dao.DaoWithHistory;
import org.butor.json.CommonRequestArgs;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class AuthDaoWithHistoryImpl extends org.butor.auth.dao.AuthDaoImpl implements DaoWithHistory {
	@Override
	public Object getRowForHistory(MapSqlParameterSource params) {
		CommonRequestArgs cra = new CommonRequestArgs();
		cra.setUserId((String)params.getValue("userId"));
		cra.setLang((String)params.getValue("lang"));
		cra.setReqId((String)params.getValue("reqId"));
		cra.setSessionId((String)params.getValue("sessionId"));

		int authId = (Integer)params.getValue("authId");
		return readAuth(authId, cra);
	}
	@Override
	public String getInsertSql() {
		return super.getInsertAuthSql();
	}
}
