/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.butor.auth.common.AuthDataFilter;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.firm.Firm;
import org.butor.auth.common.firm.FirmKey;
import org.butor.auth.common.firm.FirmWithAccessMode;
import org.butor.auth.common.firm.ListFirmCriteria;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.dao.DaoWithHistory;
import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonHelper;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.util.StringUtils;

import com.google.common.base.Strings;

public class FirmDaoImpl extends AbstractDao implements FirmDao, DaoWithHistory {

	private String listSql;
	private String readSql;
	private String deleteSql;
	private String updateSql;
	private String insertSql;

	private AuthDao authDao;

	private final String PROC_LIST_FIRM = getClass().getName() +".listFirm";
	private final String PROC_READ_FIRM = getClass().getName() +".readFirm";
	private final String PROC_INSERT_FIRM = getClass().getName() +".insertFirm";
	private final String PROC_UPDATE_FIRM = getClass().getName() +".updateFirm";
	private final String PROC_DELETE_FIRM = getClass().getName() +".deleteFirm";

	@Override
	public List<FirmWithAccessMode> listFirm(ListFirmCriteria crit, CommonRequestArgs cra) {
		if (StringUtils.hasLength(crit.getFirmName())) {
			crit.setFirmName("%" + crit.getFirmName() + "%");
		}
		AuthDataFilter authDataCrit = new AuthDataFilter();
		authDataCrit.setSys(crit.getSys() != null ? crit.getSys() : SecurityConstants.SYSTEM_ID);
		authDataCrit.setFunc(crit.getFunc() != null ? crit.getFunc() : SecurityConstants.SEC_FUNC_FIRMS);
		authDataCrit.setDataTypes(SecurityConstants.SEC_DATA_TYPE_FIRM);
		authDataCrit.setMode((crit.getMode() != null ? crit.getMode() : AccessMode.READ).value());
		authDataCrit.setMember(cra.getUserId());

		List<FirmWithAccessMode> fl = queryList(PROC_LIST_FIRM, listSql, FirmWithAccessMode.class, crit, cra, authDataCrit);
		JsonHelper jsh = new JsonHelper();
		for (FirmWithAccessMode f : fl) {
			if (f.getAttributes() != null) {
				f.setAttributesMap(jsh.deserialize(f.getAttributes(), Map.class));
			}
		}
		return fl;
	}

	public void setListSql(String listSql_) {
		this.listSql = listSql_;
	}

	public void setReadSql(String readSql) {
		this.readSql = readSql;
	}

	@Override
	public Firm readFirm(long firmId, String sys, String func, AccessMode mode, CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("firmId", firmId);

		AuthDataFilter authDataCrit = new AuthDataFilter();
		authDataCrit.setSys(Strings.isNullOrEmpty(sys) ? SecurityConstants.SYSTEM_ID : sys).
		setFunc(Strings.isNullOrEmpty(func) ? SecurityConstants.SEC_FUNC_FIRMS : func).
		setDataTypes("firm").
		setMode(mode == null ? AccessMode.READ.value() : mode.value()).
		setMember(cra.getUserId());

		Firm firm = queryFirst(PROC_READ_FIRM, readSql, Firm.class, args, cra, authDataCrit);
		if (firm != null && !Strings.isNullOrEmpty(firm.getAttributes())) {
			JsonHelper jsh = new JsonHelper();
			firm.setAttributesMap(jsh.deserialize(firm.getAttributes(), Map.class));
		}
		return firm;
	}
	private Firm readFirm(long firmId, CommonRequestArgs cra) {
		return readFirm(firmId, null, null, null, cra);
	}
	@Override
	public FirmKey insertFirm(Firm firm, CommonRequestArgs cra) {
		if (!authDao.hasAccess(SecurityConstants.SYSTEM_ID, SecurityConstants.SEC_FUNC_FIRMS, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		
		//TODO remove 'theme' field from DB
		firm.setTheme("");

		firm.setStamp(new Date());
		firm.setCreationDate(firm.getStamp());
		firm.setRevNo(0);

		if (firm.getAttributesMap() != null) {
			JsonHelper jsh = new JsonHelper();
			firm.setAttributes(jsh.serialize(firm.getAttributesMap()));
		}

		UpdateResult ur = insert(PROC_INSERT_FIRM, insertSql, firm, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		return new FirmKey(ur.key, 0);
	}

	@Override
	public FirmKey updateFirm(Firm firm, CommonRequestArgs cra) {
		if (!authDao.hasAccess(SecurityConstants.SYSTEM_ID, SecurityConstants.SEC_FUNC_FIRMS, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		
		//TODO remove 'theme' field from DB
		firm.setTheme("");
		
		if (firm.getAttributesMap() != null) {
			JsonHelper jsh = new JsonHelper();
			firm.setAttributes(jsh.serialize(firm.getAttributesMap()));
		}
		UpdateResult ur = update(PROC_UPDATE_FIRM, updateSql, firm, cra);
		if (ur.numberOfRowAffected == 0) {
			Firm ef = readFirm(firm.getFirmId(), cra);
			if (ef == null || ef.getRevNo() != firm.getRevNo()) {
				ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
			} else {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}
		}
		return new FirmKey(firm.getFirmId(), firm.getRevNo() +1);
	}

	@Override
	public void deleteFirm(FirmKey firmKey, CommonRequestArgs cra) {
		if (!authDao.hasAccess(SecurityConstants.SYSTEM_ID, SecurityConstants.SEC_FUNC_FIRMS, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}

		UpdateResult ur = delete(PROC_DELETE_FIRM, deleteSql, firmKey, cra);
		if (ur.numberOfRowAffected == 0) {
			Firm ef = readFirm(firmKey.getFirmId(), cra);
			if (ef == null || ef.getRevNo() != firmKey.getRevNo()) {
				ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
			} else {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}
		}
	}

	@Override
	public Object getRowForHistory(MapSqlParameterSource params) {
		CommonRequestArgs cra = new CommonRequestArgs();
		cra.setUserId((String)params.getValue("userId"));
		cra.setLang((String)params.getValue("lang"));
		cra.setReqId((String)params.getValue("reqId"));
		cra.setSessionId((String)params.getValue("sessionId"));
		long firmId = (Long)params.getValue("firmId");

		return readFirm(firmId, null, null, null, cra);
	}
	@Override
	public String getInsertSql() {
		return insertSql;
	}
	public void setUpdateSql(String updateSql) {
		this.updateSql = updateSql;
	}

	public void setInsertSql(String insertSql) {
		this.insertSql = insertSql;
	}

	public void setDeleteSql(String deleteSql) {
		this.deleteSql = deleteSql;
	}

	public void setAuthDao(AuthDao authDao) {
		this.authDao = authDao;
	}
}