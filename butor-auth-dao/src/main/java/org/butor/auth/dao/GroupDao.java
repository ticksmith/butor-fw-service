/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.List;

import org.butor.auth.common.group.Group;
import org.butor.auth.common.group.GroupItem;
import org.butor.auth.common.group.GroupItemKey;
import org.butor.auth.common.group.GroupKey;
import org.butor.json.CommonRequestArgs;

public interface GroupDao {
	GroupItem readItem(GroupItemKey gik, CommonRequestArgs cra);
	void updateGroup(Group group, CommonRequestArgs cra);
	List<GroupItem> readGroup(String groupId, CommonRequestArgs cra);
	GroupItemKey insertItem(GroupItem item, CommonRequestArgs cra);
	void deleteItem(GroupItemKey gik, CommonRequestArgs cra);
	void deleteGroup(GroupKey groupKey, CommonRequestArgs cra);
	boolean isGroupRefered(String groupId, CommonRequestArgs cra);
	List<Group> listGroup(String member, String func, CommonRequestArgs cra);
}
