/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import org.butor.json.CommonRequestArgs;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.base.Preconditions;
import com.google.common.reflect.Reflection;
/**
 * Factory class that return a proxy of the service interface
 * that calls the model instance
 * 
 * This eliminate the need of creating a new service class
 * that only calls the model. 
 * 
 * It supports transaction by adding the @Transactional annotation to 
 * the model. 
 * 
 * In order to support transactions, a PlatformTransactionManager 
 * has to be provided to this class 
 *  
 * @author tbussier
 *
 * @param <T>
 */
@Deprecated
public class ServiceFactory<T> implements InitializingBean, FactoryBean<T>{
	
	public PlatformTransactionManager platformTransactionManager;
	public TransactionTemplate trxTpl;
	public Object modelInstance;
	public Class<T> serviceInterface;
	public T proxy;
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		Preconditions.checkNotNull(modelInstance,"modelInstance is mandatory");
		Preconditions.checkNotNull(serviceInterface, "serviceInterface is mandatory");
		proxy = buildProxy(serviceInterface,modelInstance);
		if (platformTransactionManager != null) {
			trxTpl = new TransactionTemplate(platformTransactionManager);
		}
		
	}

	@Override
	public T getObject() throws Exception {
		return proxy;
	}

	@Override
	public Class<?> getObjectType() {
		return serviceInterface;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public void setModelInstance(Object modelInstance) {
		this.modelInstance = modelInstance;
	}

	public void setServiceInterface(Class<T> serviceInterface) {
		this.serviceInterface = serviceInterface;
	}

	private T buildProxy(Class<T> serviceInterface, Object modelInstance) {
	
		Map<String, Method> modelMethodMap = new HashMap<String, Method>();
		Class<?> modelClass = modelInstance.getClass();
		for (Method m : modelClass.getMethods()) {
			String modelMethodName = m.getName();
			if (Modifier.isPublic(m.getModifiers())) {
				Class<?>[] modelParamTypes = m.getParameterTypes();
				if (modelParamTypes.length > 2
						&& ResponseHandler.class.isAssignableFrom(modelParamTypes[modelParamTypes.length - 1])
						&& CommonRequestArgs.class.isAssignableFrom(modelParamTypes[modelParamTypes.length - 2])) {
					Preconditions
							.checkArgument(
									!modelMethodMap.containsKey(modelMethodName),
									"Service Wrapper does not support 2 public methods with the same name with first parameter CommonRequestArgs! %s",
									modelClass);
					modelMethodMap.put(modelMethodName, m);
	
				}
			}
		}
		Preconditions.checkArgument(serviceInterface.isInterface(), "This class : %s must be an interface",
				serviceInterface);
		Map<String, Method> validModelMethodMap = new HashMap<String, Method>();
		for (Method m : serviceInterface.getMethods()) {
			String serviceMethodName = m.getName();
			Class<?>[] serviceParamTypes = m.getParameterTypes();
			if (serviceParamTypes.length > 1 && Context.class.isAssignableFrom(serviceParamTypes[0])) {
				Method modelMethod = modelMethodMap.get(serviceMethodName);
				Preconditions.checkNotNull(modelMethod, "Method %s not found in model %s", serviceMethodName,
						modelClass);
				Class<?>[] modelParamTypes = modelMethod.getParameterTypes();
				Preconditions.checkArgument((serviceParamTypes.length + 1 == modelParamTypes.length),
						"Invalid number of parameter for model %s method %s", serviceParamTypes.length,
						modelMethod.getParameterTypes().length);
	
				
				for (int i=1;i<serviceParamTypes.length;i++) {
					Preconditions.checkArgument(serviceParamTypes[i].isAssignableFrom(modelParamTypes[i-1]), "Service parameter number %d, is not compatible with model parameter %d",i,i);
					
				}
				if (modelMethod.isAnnotationPresent(Transactional.class)) {
					Preconditions.checkNotNull(trxTpl,"Method %s of model %s is transactionnal, but no PlatformTransactionManager were provided to this ServiceFactory!",modelMethod.getName(),modelClass);
				}
				validModelMethodMap.put(serviceMethodName, modelMethod);
			}
		}
		return Reflection.newProxy(serviceInterface, new ServiceWrapperInvokationHandler(validModelMethodMap));
	
	}

	private class ServiceWrapperInvokationHandler implements InvocationHandler {
		final Map<String, Method> modelMethodMap;
	
		public ServiceWrapperInvokationHandler(Map<String, Method> modelMethodMap) {
			this.modelMethodMap = modelMethodMap;
		}
	
		@Override
		public Object invoke(Object proxy, Method method, Object[] serviceParams) throws Throwable {
			String methodName = method.getName();
			final Method modelMethod = modelMethodMap.get(methodName);
			Context ctx = (Context) serviceParams[0];
			final Object[] modelParams = new Object[serviceParams.length+1];
			modelParams[modelParams.length-2] = ctx.getRequest();
			modelParams[modelParams.length-1] = ctx.getResponseHandler();
			for (int i=1;i<serviceParams.length;i++) {
				modelParams[i-1] = serviceParams[i];
			}
			if (modelMethod.isAnnotationPresent(Transactional.class)) {
				trxTpl.execute(new TransactionCallbackWithoutResult(){
					@Override
					protected void doInTransactionWithoutResult(TransactionStatus status) {
						try {
							modelMethod.invoke(modelInstance, modelParams);
						} catch (Throwable e) {
							status.setRollbackOnly();
						}
					}});
				return null;
			} else {
				return modelMethod.invoke(modelInstance, modelParams);
			}
		}
	
		@Override
		public String toString() {
			return String.format("ServiceWrapperInvokationHandler for service interface %s for invoking methods in model %s",serviceInterface,modelInstance.getClass());
		}
	
	}

	public void setPlatformTransactionManager(PlatformTransactionManager transactionManager) {
		this.platformTransactionManager = transactionManager;
	}


}
