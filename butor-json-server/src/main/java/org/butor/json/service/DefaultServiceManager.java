/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonHelper;
import org.butor.json.JsonServiceRequest;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.butor.utils.Message;
import org.butor.utils.Message.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.gson.JsonParser;

public class DefaultServiceManager implements ServiceManager,InitializingBean,BeanFactoryAware {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private JsonHelper jsh = new JsonHelper();
	private JsonParser jsonParser = new JsonParser();
	
	private PlatformTransactionManager transactionManager;
	private BeanFactory beanFactory;
	
	private ConcurrentMap<String, ServiceComponent> cmpRegistry = 
			new ConcurrentHashMap<String, ServiceComponent>();

	private List<ServiceComponent> components;
	
	private boolean allowHtmlTagsInServicesArgs = true;

	@Override
	public void registerServices(List<ServiceComponent> components) {
		for (ServiceComponent ser : components)
			registerService(ser);
	}

	@Override
	public void registerService(ServiceComponent serviceCmp) {
		String ns = serviceCmp.getNamespace();
		if (cmpRegistry.containsKey(ns)) {
			logger.error(String.format("Namespace %s has been used by component %s", 
					ns, cmpRegistry.get(ns).toString()));
			return;
		}
		cmpRegistry.put(ns, serviceCmp);
	}

	@Override
	public void unregisterService(ServiceComponent cmp) {
		cmpRegistry.remove(cmp.getNamespace());
	}

	@Override
	public boolean isBinary(JsonServiceRequest req) {
		final ServiceComponent cmp = cmpRegistry.get(req.getNamespace());
		if (cmp == null) {
			String msg = String.format("No service component found with ns=%s", req.getNamespace());
			logger.info(msg);
			return false;
		}
		return cmp.isBinary();
	}
	@Override
	public void invoke(final Context context) {
		JsonServiceRequest req = (JsonServiceRequest)context.getRequest();
		final String namespace = req.getNamespace();
		final ServiceComponent cmp = cmpRegistry.get(namespace);
		if (cmp == null) {
			String msg = String.format("No service component found with ns=%s", namespace);
			logger.info(msg);
			return;
		}

		String args = req.getServiceArgsJson();
		if (!allowHtmlTagsInServicesArgs) {
			args = args.replaceAll("u003c|<", "&lt;").replaceAll("u003e|<", "&gt;");
		}

		Object[] params  = Iterables.toArray(jsonParser.parse(req.getServiceArgsJson()).getAsJsonArray(), Object.class);

		int nbArgs = params.length +1; // +1 for context arg

		final String serviceName = req.getService();
		final Method serviceMethod = cmp.getService(serviceName, nbArgs);

		if (serviceMethod == null) {
			String msg = String.format("No service=%s found with ns=%s", 
					serviceName, namespace);
			logger.info(msg);
			context.getResponseHandler().addMessage(new Message(0, MessageType.ERROR, msg));
			return;
		}

		try {
			final CommonRequestArgs cr = new CommonRequestArgs();
			cr.setLang(req.getLang());
			cr.setReqId(req.getReqId());
			cr.setSessionId(req.getSessionId());
			cr.setUserId(req.getUserId());
			cr.setDomain(req.getDomain());
			Context ictx = new Context() {
				@Override
				public ResponseHandler<?> getResponseHandler() {
					return context.getResponseHandler();
				}
				@Override
				public CommonRequestArgs getRequest() {
					return cr;
				}
			};
			
			Class<?>[] pts = serviceMethod.getParameterTypes();
			final Object[] serviceParameters = new Object[pts.length];
			serviceParameters[0] = ictx;
			for (int ii=1; ii<pts.length; ii++) {
				String par = params[ii-1].toString();			
				serviceParameters[ii] = jsh.deserialize(par, pts[ii]);
			}
			if (serviceMethod.isAnnotationPresent(Transactional.class)) {
				Transactional trx = serviceMethod.getAnnotation(Transactional.class);
				Preconditions.checkNotNull(transactionManager,"The method is transactionnal, but no transaction manager was detected!");
				TransactionTemplate trxTpl = new TransactionTemplate(transactionManager);
				trxTpl.setIsolationLevel(trx.isolation().value());
				trxTpl.setReadOnly(trx.readOnly());
				trxTpl.setPropagationBehavior(trx.propagation().value());
				trxTpl.setTimeout(trx.timeout());
				trxTpl.execute(new TransactionCallbackWithoutResult() {
					@Override
					protected void doInTransactionWithoutResult(TransactionStatus status) {
						try {
							serviceMethod.invoke(cmp.getComponent(), serviceParameters);
						} catch (Throwable e) {
							status.setRollbackOnly();
							if (e instanceof InvocationTargetException) {
								handleException(context, namespace, serviceName, ((InvocationTargetException)e).getTargetException());
							} else {
								handleException(context, namespace, serviceName, e);
							}
						}
					}});
				
			} else {
					serviceMethod.invoke(cmp.getComponent(), serviceParameters);
			}			
			
		} catch (InvocationTargetException e) {
			handleException(context, namespace, serviceName, e.getTargetException());
		} catch (Throwable e) {
			handleException(context, namespace, serviceName, e);
		}
	}

	private void handleException(final Context ctx, String ns, String serviceName, Throwable e) {
		if (e instanceof ApplicationException) {
			ApplicationException appEx = (ApplicationException)e;
			logger.warn(String.format("Failed to invoke service e=%s, ns=%s", 
					serviceName, ns), appEx);
			Message[] ml = appEx.getMessages();
			if (ml == null||ml.length ==0) {
				ctx.getResponseHandler().addMessage(CommonMessageID.SERVICE_FAILURE.getMessage(appEx.getMessage()));
			} else {
				for (Message message : appEx.getMessages()) {
					ctx.getResponseHandler().addMessage(message);
				}
			}
			
		} else {
			logger.error(String.format("Failed to invoke service=%s, ns=%s", 
					serviceName, ns), e);
			ctx.getResponseHandler().addMessage(CommonMessageID.SERVICE_FAILURE.getMessage(e.getMessage()));
		}
	}

	public List<ServiceComponent> getComponents() {
		return components;
	}

	public void setComponents(List<ServiceComponent> components_) {
		components = components_;
		registerServices(components_);
	}


	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			transactionManager = beanFactory.getBean(PlatformTransactionManager.class);
		} catch (NoSuchBeanDefinitionException e) {
			logger.warn("No (or more than one) TransactionManager defined in your Spring context. Will not set transaction manager.",e);
		}
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
		
	}

	public void setAllowHtmlTagsInServicesArgs(boolean allowHtmlTagsInServicesArgs) {
		this.allowHtmlTagsInServicesArgs = allowHtmlTagsInServicesArgs;
	}
}
