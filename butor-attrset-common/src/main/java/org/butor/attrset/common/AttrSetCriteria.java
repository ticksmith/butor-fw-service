/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.common;

public class AttrSetCriteria {
	private String type;
	private String id;
	private String k1;
	private String k2;
	public String getId() {
		return id;
	}
	public void setId(String id_) {
		id = id_;
	}
	@Override
	public String toString() {
		return "AttrSet [id=" + id + ", k1=" + k1 + ", k2=" + k2 + "]";
	}
	public String getK1() {
		return k1;
	}
	public void setK1(String k1_) {
		k1 = k1_;
	}
	public String getK2() {
		return k2;
	}
	public void setK2(String k2_) {
		k2 = k2_;
	}
	public String getType() {
		return type;
	}
	public void setType(String type_) {
		type = type_;
	}
	
	public static AttrSetCriteria valueOf(String type, String id, String k1, String k2) {
		AttrSetCriteria criteria = new AttrSetCriteria();
		criteria.setType(type);
		criteria.setId(id);
		criteria.setK1(k1);
		criteria.setK2(k2);
		return criteria;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((k1 == null) ? 0 : k1.hashCode());
		result = prime * result + ((k2 == null) ? 0 : k2.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttrSetCriteria other = (AttrSetCriteria) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (k1 == null) {
			if (other.k1 != null)
				return false;
		} else if (!k1.equals(other.k1))
			return false;
		if (k2 == null) {
			if (other.k2 != null)
				return false;
		} else if (!k2.equals(other.k2))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
	
}
