/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.butor.attrset.common.AttrSet;
import org.butor.attrset.dao.AttrSetDao;
import org.butor.attrset.util.Attributes;
import org.butor.attrset.util.Attributes.AttributesBuilder;
import org.butor.auth.common.AuthMessageID;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.user.ListUserCriteria;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserKey;
import org.butor.auth.common.user.UserModel;
import org.butor.auth.common.user.UserQuestions;
import org.butor.auth.common.user.UserServices;
import org.butor.auth.dao.UserDao;
import org.butor.checksum.CommonChecksumFunction;
import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonHelper;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.ldap.LdapUserModel;
import org.butor.mail.EmailTemplate;
import org.butor.mail.IMailer;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.butor.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultUserModel implements UserServices, UserModel {
	protected UserDao userDao;
	private Logger logger = LoggerFactory.getLogger(getClass());

	protected IMailer mailer;
	protected String fromRecipient;
	protected AttrSetDao attrSetDao;
	protected UserValidator userValidator = new DefaultUserValidator();

	@Override
	public void readUser(Context<User> ctx, String id, String func) {
		ResponseHandler<User> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		User user = readUser(id, func, cra);
		if (user == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(user);
	}

	@Override
	public void insertUser(Context<UserKey> ctx, User user) {
		ResponseHandler<UserKey> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		UserKey uk = insertUser(user, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.SERVICE_FAILURE.getMessage());
			return;
		}

		rh.addRow(uk);
	}

	@Override
	public void updateUser(Context<UserKey> ctx, User user) {
		ResponseHandler<UserKey> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		UserKey uk = updateUser(user, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(uk);
	}

	@Override
	public void deleteUser(Context<Void> ctx, UserKey userKey) {
		CommonRequestArgs cra = ctx.getRequest();
		deleteUser(userKey, cra);
	}

	@Override
	public void listUser(Context<User> ctx, ListUserCriteria criteria,
			String func) {
		CommonRequestArgs cra = ctx.getRequest();
		ResponseHandler<User> rh = ctx.getResponseHandler();
		List<User> list = listUser(criteria, func, cra);
		Iterator<User> it = list.iterator();
		while (it.hasNext()) {
			User u = it.next();
			u.setPwd(null);
			rh.addRow(u);
		}
	}

	@Override
	public void readQuestions(Context<UserQuestions> ctx, String id) {
		ResponseHandler<UserQuestions> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		UserQuestions uq = readQuestions(id, cra);
		if (uq == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(uq);
	}

	@Override
	public void updateQuestions(Context<UserKey> ctx, UserQuestions questions) {
		ResponseHandler<UserKey> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		UserKey uk = updateQuestions(questions, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(uk);
	}

	@Override
	public void updateState(Context<UserKey> ctx, User user) {
		ResponseHandler<UserKey> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		UserKey uk = updateState(user, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(uk);
	}

	@Override
	public void resetLogin(Context<String> ctx, String id, String domain,
			boolean resetAndSendLink) {
		resetLogin(ctx, id, domain, true, resetAndSendLink);
	}

	@Override
	public void resetLogin(Context<String> ctx, String id, String domain,
			boolean resetQuestions, boolean resetAndSendLink) {
		ResponseHandler<String> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();

		String url = resetLogin(id, domain, resetQuestions, resetAndSendLink,
				cra);
		rh.addRow(url);
	}

	// UserModel ==========================

	@Override
	public User readUser(String id, String func, CommonRequestArgs cra) {
		return userDao.readUser(id, func, cra);
	}

	@Override
	public UserKey insertUser(User user, CommonRequestArgs cra) {
		validateUser(user);
		if (isNullOrEmpty(user.getFullName())) {
			user.setFullName(user.getDisplayName());
		}

		if (!isNullOrEmpty(user.getNewPwd())) {
			user.setPwd(CommonChecksumFunction.SHA512.generateChecksum(user
					.getNewPwd()));
		}

		return userDao.insertUser(user, cra);
	}

	protected void validateUser(User user) {
		userValidator.validateUser(user);
	}

	protected String getUserLang(User user, CommonRequestArgs cra) {
		String lang = user.getLanguage();
		if (isNullOrEmpty(lang)) {
			// user lang is in his profile
			Attributes attrs = new AttributesBuilder().setType("user")
					.setId(user.getId()).setAttrSetDao(attrSetDao)
					.setCommonRequestArgs(cra).build();

			if (attrs != null) {
				lang = attrs.get("language", ".");
			}
		}
		return lang;
	}

	protected EmailTemplate getEmailTemplate(String type, String lang,
			CommonRequestArgs cra) {
		Attributes attrs = new AttributesBuilder().setType(type)
				.setId("email-template").setAttrSetDao(attrSetDao)
				.setCommonRequestArgs(cra).build();

		if (isNullOrEmpty(lang)) {
			lang = cra.getLang();
		}
		EmailTemplate det = null;
		EmailTemplate et = null;
		Collection<AttrSet> list = attrs.list();
		for (AttrSet as : list) {
			if (as.getK1().equalsIgnoreCase(cra.getDomain())
					&& as.getK2().equalsIgnoreCase(lang)) {

				et = new JsonHelper().deserialize(as.getValue(),
						EmailTemplate.class);
				break;
			}
			// any default ?
			if (as.getK1().equals("*") && as.getK2().equalsIgnoreCase(lang)) {

				det = new JsonHelper().deserialize(as.getValue(),
						EmailTemplate.class);
			}
		}

		if (et == null) {
			et = det;
			if (et == null) {
				et = new EmailTemplate();
				et.setFromRecipient(fromRecipient);
				if (lang.equals("fr")) {
					et.setSubject("Reinitialisation login portail");
					et.setMessage("Votre login au portail a été reinitialisé à votre demande.\n"
							+ "SVP cliquer sur le lien plus bas et suivre les instructions.\n\n"
							+ "{link}");
				} else {
					et.setSubject("Portal login reset");
					et.setMessage("Your login to the portal has been reset as you requested.\n"
							+ "Please click on the link bellow and follow the instructions.\n\n"
							+ "{link}");
				}
			}
		}

		return et;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setLdapUserModel(LdapUserModel ldapUserModel) {
		logger.warn("LdapUserModel is not used!");
	}

	public void setMailer(IMailer mailer) {
		this.mailer = mailer;
	}

	public void setFromRecipient(String fromRecipient) {
		this.fromRecipient = fromRecipient;
	}

	public void setAttrSetDao(AttrSetDao attrSetDao) {
		this.attrSetDao = attrSetDao;
	}

	@Override
	public List<User> listUser(ListUserCriteria criteria, String func,
			CommonRequestArgs cra) {
		return userDao.listUser(criteria, func, cra);
	}

	@Override
	public UserKey updateUser(User user, CommonRequestArgs cra) {
		validateUser(user);
		if (isNullOrEmpty(user.getFullName())) {
			user.setFullName(user.getDisplayName());
		}

		if (!isNullOrEmpty(user.getNewPwd())) {
			user.setPwd(CommonChecksumFunction.SHA512.generateChecksum(user
					.getNewPwd()));
		}
		return userDao.updateUser(user, cra);
	}

	@Override
	public void deleteUser(UserKey userKey, CommonRequestArgs cra) {
		User user = userDao.readUser(userKey.getId(), null, cra);
		if (user == null) {
			ApplicationException.exception(AuthMessageID.USER_NOT_FOUND
					.getMessage());
		}
		if (user.isActive()) {
			ApplicationException
					.exception(AuthMessageID.USER_SHOULD_BE_INACTIVE_TO_BE_DELETED
							.getMessage());
		}
		userDao.deleteUser(userKey, cra);
	}

	@Override
	public UserQuestions readQuestions(String id, CommonRequestArgs cra) {
		return userDao.readQuestions(id, cra);
	}

	@Override
	public UserKey updateQuestions(UserQuestions questions,
			CommonRequestArgs cra) {
		return userDao.updateQuestions(questions, cra);
	}

	@Override
	public UserKey updateState(User user, CommonRequestArgs cra) {
		return userDao.updateState(user, cra);
	}

	@Override
	public String resetLogin(String id, String domain,
			boolean resetAndSendLink, CommonRequestArgs cra) {
		return resetLogin(id, domain, true, resetAndSendLink, cra);
	}

	@Override
	public String resetLogin(String id, String domain, boolean resetQuestions,
			boolean resetAndSendLink, CommonRequestArgs cra) {

		if (mailer == null && resetAndSendLink) {
			logger.warn("Mailer in not set!");
			throw ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}
		if (StringUtil.isEmpty(id)) {
			logger.warn("Missing credential id arg");
			throw ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}

		User user = userDao.readUser(id, SecurityConstants.SEC_FUNC_USERS, cra);
		if (user == null) {
			logger.warn("Unknown user {}", id);
			ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}
		if (StringUtil.isEmpty(user.getEmail())) {
			logger.warn(String.format("No user defined for id %s", id));
			ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}

		String tokSeed = UUID.randomUUID().toString();

		// gen token for reset
		String resetToken = CommonChecksumFunction.SHA256
				.generateChecksumWithTTL(tokSeed, 2, TimeUnit.HOURS);
		user.setPwd(resetToken);
		user.setActive(false);
		user.setResetInProgress(resetQuestions);
		user.setMissedLogin(0);
		/*
		 * if (!CommonChecksumFunction.SHA512.validateChecksum(cred.getPwd(),
		 * user.getPwd())) {
		 * logger.warn(String.format("Password do not match for user=%s",
		 * cred.getEmail())); throw
		 * ApplicationException.exception(PortalMessageID
		 * .RESET_PWD_FAILED.getMessage()); }
		 */
		UserKey uk = userDao.updateUser(user, cra);
		if (uk.getId() == null) {
			logger.warn(String.format(
					"Failed to save new password for user=%s", id));
			ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}

		String url = domain;
		if (isNullOrEmpty(url)) {
			url = cra.getDomain();
		}

		url += "/reset?t=" + resetToken + "&id=" + user.getId();

		String lang = getUserLang(user, cra);

		EmailTemplate et = getEmailTemplate("reset-login", lang, cra);
		String msg = et.getMessage();
		msg = msg.replace("{username}", user.getDisplayName());
		msg = msg.replace("{link}", url);
		String fromRecipient = et.getFromRecipient();
		if (isNullOrEmpty(fromRecipient)) {
			fromRecipient = this.fromRecipient;
		}
		logger.info(String.format(
				"Sending reset link via email for user=%s\n%s", id, msg));

		if (resetAndSendLink) {
			mailer.sendMail(user.getEmail(), et.getSubject(), msg,
					fromRecipient);
		} else {
			return url;
		}
		return null;
	}

	public void setUserValidator(UserValidator userValidator) {
		this.userValidator = userValidator;
	}
}
