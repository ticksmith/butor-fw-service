/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import java.util.List;

import org.butor.auth.common.func.Func;
import org.butor.auth.common.func.FuncKey;
import org.butor.auth.common.func.FuncServices;
import org.butor.auth.dao.FuncDao;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.json.service.ResponseHandlerHelper;
import org.butor.utils.CommonMessageID;
import org.springframework.transaction.annotation.Transactional;

public class DefaultFuncModel implements FuncServices {
	private FuncDao funcDao;

	@Override
	public void listFunc(Context<Func> ctx, String sys, String func) {
		ResponseHandler<Func> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		List<Func> list = funcDao.listFunc(sys, func, cra);
		ResponseHandlerHelper.addList(list, rh);
	}

	@Override
	public void readFunc(Context ctx, FuncKey funcKey) {
		ResponseHandler<Object> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		Func func = funcDao.readFunc(funcKey, cra);
		if (func == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(func);
	}

	@Override
	@Transactional
	public void updateFunc(Context ctx, Func func) {
		ResponseHandler<Object> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		FuncKey uk = funcDao.updateFunc(func, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(uk);
	}

	public void setFuncDao(FuncDao funcDao) {
		this.funcDao = funcDao;
	}


	@Override
	@Transactional
	public void insertFunc(Context ctx, Func func) {
		ResponseHandler<Object> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		FuncKey uk = funcDao.insertFunc(func, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.SERVICE_FAILURE.getMessage());
			return;
		}
		rh.addRow(uk);
	}

	@Override
	@Transactional
	public void deleteFunc(Context ctx, FuncKey uk) {
		CommonRequestArgs cra = ctx.getRequest();
		funcDao.deleteFunc(uk, cra);
	}
}
