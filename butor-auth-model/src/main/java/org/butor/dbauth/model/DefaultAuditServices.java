/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import java.util.List;

import org.butor.auth.common.audit.Audit;
import org.butor.auth.common.audit.AuditCriteria;
import org.butor.auth.common.audit.AuditServices;
import org.butor.auth.common.audit.ServiceCall;
import org.butor.auth.common.audit.ServiceCallCriteria;
import org.butor.auth.dao.AuditDao;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.json.service.ResponseHandlerHelper;
import org.butor.utils.CriteriaSanitizer;

public class DefaultAuditServices implements AuditServices {

	private AuditDao auditDao;

	private final CriteriaSanitizer<AuditCriteria> auditCriteriaSanitizer = new CriteriaSanitizer<AuditCriteria>();
	private CriteriaSanitizer<ServiceCallCriteria> serviceCallCriteriaSanitizer = new CriteriaSanitizer<ServiceCallCriteria>();

	@Override
	public void listAudit(Context<Audit> ctx, AuditCriteria criteria) {
		CommonRequestArgs cra = ctx.getRequest();
		auditCriteriaSanitizer.clean(criteria);
		ResponseHandler<Audit> rh = ctx.getResponseHandler();
		List<Audit> auditList = auditDao.listAudit(criteria, cra);
		ResponseHandlerHelper.addList(auditList, rh);

	}

	@Override
	public void listServiceCall(Context<ServiceCall> ctx,
			ServiceCallCriteria criteria) {
		CommonRequestArgs cra = ctx.getRequest();
		serviceCallCriteriaSanitizer.clean(criteria);
		ResponseHandler<ServiceCall> rh = ctx.getResponseHandler();
		List<ServiceCall> serviceCalls = auditDao.listServiceCalls(criteria,
				cra);
		ResponseHandlerHelper.addList(serviceCalls, rh);

	}

	public void setAuditDao(AuditDao auditDao) {
		this.auditDao = auditDao;
	}

}
