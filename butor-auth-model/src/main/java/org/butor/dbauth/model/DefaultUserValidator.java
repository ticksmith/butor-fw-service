/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import static com.google.common.base.Strings.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.butor.auth.common.AuthMessageID;
import org.butor.auth.common.user.User;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;

public class DefaultUserValidator implements UserValidator {
	public static final String EMAIL_REGEX = "^[a-zA-Z0-9\\w\\.-]+@[a-zA-Z0-9\\w\\.-]+\\.[a-zA-Z0-9\\w\\.-]+$";
	public static final Pattern emailPattern = Pattern.compile(EMAIL_REGEX);

	@Override
	public void validateUser(User user) {
		if (user == null) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("User"));
		}
		if (isNullOrEmpty(user.getId())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("ID"));
		}
		if (isNullOrEmpty(user.getEmail())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Email"));
		}
		Matcher matcher = emailPattern.matcher(user.getEmail());
		if (!matcher.find()) {
			ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("Email"));
		}
		if (isNullOrEmpty(user.getFirstName())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("First name"));
		}
		if (isNullOrEmpty(user.getLastName())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Last name"));
		}
		if (isNullOrEmpty(user.getDisplayName())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Display name"));
		}
		if (user.getFirmId() <= 0) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Firm"));
		}
		if (!isNullOrEmpty(user.getNewPwd()) || !isNullOrEmpty(user.getNewPwdConf())) {
			if (isNullOrEmpty(user.getNewPwd())) {
				ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("New password"));
			}
			if (isNullOrEmpty(user.getNewPwdConf())) {
				ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Confirm new password"));
			}
			if (!user.getNewPwd().equals(user.getNewPwdConf())) {
				ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("new passwords do not match"));
			}
		}
		validateId(user);
		
	}
	protected void validateId(User user) {
		String id = user.getId();
		//force id to lower case
		user.setId(id.toLowerCase());
		boolean emailId = emailPattern.matcher(id).find();
		if (!emailId) {
			ApplicationException.exception(AuthMessageID.USER_ID_SHOULD_BE_EMAIL.getMessage());
		}
	}

}
