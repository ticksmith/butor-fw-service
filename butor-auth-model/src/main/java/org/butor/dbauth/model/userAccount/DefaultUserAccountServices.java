/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package org.butor.dbauth.model.userAccount;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.butor.auth.common.AuthModel;
import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.firm.FirmWithAccessMode;
import org.butor.auth.common.firm.ListFirmCriteria;
import org.butor.auth.common.group.Group;
import org.butor.auth.common.group.GroupItem;
import org.butor.auth.common.group.GroupModel;
import org.butor.auth.common.user.ListUserCriteria;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserModel;
import org.butor.auth.common.userAccount.UserAccountInfo;
import org.butor.auth.common.userAccount.UserAccountServices;
import org.butor.auth.dao.FirmDao;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.utils.ApplicationException;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

/**
 * @author asawan
 *
 */
public class DefaultUserAccountServices implements UserAccountServices {

	private UserModel userModel;
	private FirmDao firmDao;
	private GroupModel groupModel;
	private AuthModel authModel;
	
	public DefaultUserAccountServices(UserModel userModel,
			FirmDao firmDao,
			GroupModel groupModel,
			AuthModel authModel) {
		this.userModel = checkNotNull(userModel);
		this.firmDao = checkNotNull(firmDao);
		this.groupModel = checkNotNull(groupModel);
		this.authModel = checkNotNull(authModel);
	}
	@Transactional
	@Override
	public void createUserAccount(Context<String> ctx, UserAccountInfo userAccountInfo) {
		/*
		 * ACCESS will be checked by each of the services used bellow
		 */
		CommonRequestArgs cra = ctx.getRequest();
		
		/*
		 * validate firm
		 */
		Long firmId = getFirmId(userAccountInfo.getClientsFirmName(), cra);
		if (firmId == null) {
			ApplicationException.exception("Firm not found with name=" +userAccountInfo.getClientsFirmName());
		}
		
		/*
		 * create user
		 */
		User user = userAccountInfo.getUser();
		String id = user.getId();
		
		if (userExists(id, cra)) {
			ApplicationException.exception("User already exists id=" +id);
		}
		user.setFirmId(firmId);
		user.setFirmName(userAccountInfo.getClientsFirmName());
		
		userModel.insertUser(user, cra);
		
		/*
		 * add user to groups
		 */
		if (userAccountInfo.getGroups() != null) {
			for (String groupId : userAccountInfo.getGroups()) {
				Group g = groupModel.readGroup(groupId, cra);
				if (g == null) {
					ApplicationException.exception("Group not found with id=" +groupId);
				}
				boolean alreadyMember = false;
				for (GroupItem gi  : g.getItems()) {
					if (id.equals(gi.getUserId())) {
						alreadyMember = false;
						break;
					}
				}
				if (!alreadyMember) {
					GroupItem gi = new GroupItem();
					gi.setGroupId(groupId);
					gi.setUserId(id);
					gi.setMember(id);
					if (g.getItems().isEmpty()) { // empty and Immutable list
						List<GroupItem> items = Lists.newArrayList();
						g.setItems(items);
					}
					g.getItems().add(gi);
					groupModel.updateGroup(g, cra);
				}
			}
		}

		/*
		 * add authorisations
		 */
		if (userAccountInfo.getAuths() != null) {
			for (Auth auth : userAccountInfo.getAuths()) {
				authModel.createAuth(auth, cra);
			}
		}

		/*
		 * reset login
		 */
		if (userAccountInfo.isResetLogin()) {
			String resetUrl = userModel.resetLogin(id, userAccountInfo.getResetLoginUrl(), userAccountInfo.isResetQuestions(), userAccountInfo.isResetAndSendLink(), cra);
			ctx.getResponseHandler().addRow(resetUrl);
		}
	}
	private Long getFirmId(String firmName, CommonRequestArgs cra) {
		ListFirmCriteria criteria = new ListFirmCriteria();
		criteria.setFirmName(firmName);
		List<FirmWithAccessMode> fl = firmDao.listFirm(criteria, cra);
		for (FirmWithAccessMode f : fl) {
			if (f.getFirmName().equalsIgnoreCase(firmName)) {
				return f.getFirmId();
			}
		}
		return null;
	}
	@Override
	public void userExists(Context<Boolean> ctx, String id) {
		CommonRequestArgs cra = ctx.getRequest();
		ctx.getResponseHandler().addRow(userExists(id, cra));
	}
	public boolean userExists(String id, CommonRequestArgs cra) {
		return userModel.readUser(id, null, cra) != null;
	}
	@Override
	public void userExistsByEmail(Context<Boolean> ctx, String email) {
		CommonRequestArgs cra = ctx.getRequest();
		ctx.getResponseHandler().addRow(userExistsByEmail(email, cra));
	}
	public boolean userExistsByEmail(String email, CommonRequestArgs cra) {
		ListUserCriteria luc = new ListUserCriteria();
		luc.setEmail(email);
		return userModel.listUser(luc, null, cra).size() > 0;
	}
}
