/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import java.util.List;

import org.butor.auth.common.AuthMessageID;
import org.butor.auth.common.desc.Desc;
import org.butor.auth.common.desc.DescKey;
import org.butor.auth.common.group.Group;
import org.butor.auth.common.group.GroupItem;
import org.butor.auth.common.group.GroupItemKey;
import org.butor.auth.common.group.GroupKey;
import org.butor.auth.common.group.GroupModel;
import org.butor.auth.common.group.GroupServices;
import org.butor.auth.dao.AuthDao;
import org.butor.auth.dao.DescDao;
import org.butor.auth.dao.GroupDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.json.service.ResponseHandlerHelper;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;

public class DefaultGroupModel implements GroupServices, GroupModel {
	private GroupDao groupDao;
	private DescDao descDao;
	private AuthDao authDao;
	private String systemId;
	private String secFunc;

	@Override
	public void listGroup(Context<Group> ctx, String member, String func) {
		CommonRequestArgs cra = ctx.getRequest();
		ResponseHandler<Group> rh = ctx.getResponseHandler();
		List<Group> list = groupDao.listGroup(member, func, cra);
		ResponseHandlerHelper.addList(list, rh);
	}

	@Override
	public void readGroup(Context<Group> ctx, String groupId) {
		CommonRequestArgs cra = ctx.getRequest();
		ResponseHandler<Group> rh = ctx.getResponseHandler();
		Group group = readGroup(groupId, cra);
		rh.addRow(group);
	}

	@Override
	@Transactional
	public void createGroup(Context<Group> ctx, Group group) {
		CommonRequestArgs cra = ctx.getRequest();
		if (Strings.isNullOrEmpty(group.getId())) {
			ApplicationException.exception(
					CommonMessageID.MISSING_ARG.getMessage("groupId"));
		}
		if (Strings.isNullOrEmpty(group.getDescription())) {
			ApplicationException.exception(
					CommonMessageID.MISSING_ARG.getMessage("description"));
			return;
		}
		List<GroupItem> items = group.getItems();
		Desc desc = new Desc();
		desc.setDescription(group.getDescription());
		desc.setId(group.getId());
		desc.setIdType("group");
		DescKey dk = descDao.insertDesc(desc, cra);
		if (dk == null) {
			ApplicationException.exception(
					CommonMessageID.SERVICE_FAILURE.getMessage());
			return;
		}
		for (GroupItem item : items) {
			item.setGroupId(group.getId());
			GroupItemKey uk = groupDao.insertItem(item, cra);
			if (uk == null) {
				ApplicationException.exception(
						CommonMessageID.SERVICE_FAILURE.getMessage());
				return;
			}
		}
	}

	@Override
	@Transactional
	public void updateGroup(Context<Group> ctx, Group group) {
		CommonRequestArgs cra = ctx.getRequest();
		updateGroup(group, cra);
	}

	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}

	@Override
	@Transactional
	public void deleteGroup(Context<Group> ctx, GroupKey groupKey) {
		CommonRequestArgs cra = ctx.getRequest();
		if (!authDao.hasAccess(systemId, secFunc, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		groupKey.setIdType("group");
		if (groupDao.isGroupRefered(groupKey.getId(), cra)) {
			ResponseHandler<Group> rh = ctx.getResponseHandler();
			rh.addMessage(AuthMessageID.CANNOT_DELETE_REFERED_GROUP.getMessage());
			return;
		}
		descDao.deleteDesc(groupKey, cra);
		groupDao.deleteGroup(groupKey, cra);
	}

	public void setDescDao(DescDao descDao) {
		this.descDao = descDao;
	}

	public void setAuthDao(AuthDao authDao) {
		this.authDao = authDao;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public void setSecFunc(String secFunc) {
		this.secFunc = secFunc;
	}

	@Override
	public Group readGroup(String groupId, CommonRequestArgs cra) {
		Desc desc = descDao.readDesc(new GroupKey(groupId), cra);
		if (desc == null) {
			ApplicationException.exception(CommonMessageID.NOT_FOUND.getMessage("Group"));
			return null;
		}

		Group group = new Group();
		group.setId(groupId);
		group.setDescription(desc.getDescription());
		group.setRevNo(desc.getRevNo());
		group.setStamp(desc.getStamp());
		group.setUserId(group.getUserId());
		group.setItems(groupDao.readGroup(groupId, cra));
		
		return group;
	}

	/* (non-Javadoc)
	 * @see org.butor.auth.common.group.GroupModel#updateGroup(org.butor.auth.common.group.Group, org.butor.json.CommonRequestArgs)
	 */
	@Override
	public void updateGroup(Group group, CommonRequestArgs cra) {
		if (!authDao.hasAccess(systemId, secFunc, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		if (Strings.isNullOrEmpty(group.getId())) {
			ApplicationException.exception(
					CommonMessageID.MISSING_ARG.getMessage("groupId"));
		}
		if (Strings.isNullOrEmpty(group.getDescription())) {
			ApplicationException.exception(
					CommonMessageID.MISSING_ARG.getMessage("description"));
		}
		group.setIdType("group");
		DescKey dk = descDao.updateDesc(group, cra);
		if (dk == null) {
			ApplicationException.exception(
					CommonMessageID.SERVICE_FAILURE.getMessage());
		}
		groupDao.updateGroup(group, cra);
	}
}
