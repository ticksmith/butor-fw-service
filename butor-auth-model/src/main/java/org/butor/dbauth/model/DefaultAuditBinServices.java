/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Map;

import org.butor.auth.common.audit.AuditBinServices;
import org.butor.auth.common.audit.ServiceCall;
import org.butor.auth.common.audit.ServiceCallCriteria;
import org.butor.auth.dao.AuditDao;
import org.butor.dao.RowHandler;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.BinResponseHandler;
import org.butor.json.service.Context;
import org.butor.utils.ApplicationException;
import org.butor.utils.CSVWriter;
import org.butor.utils.CSVWriterBuilder;
import org.butor.utils.CommonMessageID;
import org.butor.utils.CriteriaSanitizer;

import com.google.api.client.util.Maps;
import com.google.common.io.Closeables;

public class DefaultAuditBinServices implements AuditBinServices {

	private AuditDao auditDao;

	private final CriteriaSanitizer<ServiceCallCriteria> serviceCallCriteriaSanitizer = new CriteriaSanitizer<ServiceCallCriteria>();

	@Override
	public void exportServiceCall(Context<byte[]> ctx,
			ServiceCallCriteria criteria) {
		if (criteria == null) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG
					.getMessage("criteria"));
		}
		serviceCallCriteriaSanitizer.clean(criteria);
		CommonRequestArgs cra = ctx.getRequest();
		OutputStream os = null;
		try {
			BinResponseHandler brh = (BinResponseHandler) ctx
					.getResponseHandler();
			os = brh.getOutputStream();

			Map<String, String> headers = Maps.newHashMap();
			headers.put("Content-disposition",
					"attachment; filename=ServiceCalls.csv");
			brh.setContentType("text/csv", headers);
			os.flush();
			final PrintStream pos = new PrintStream(os);
			final CSVWriter csvWriter = getServiceCallCSVWriter();
			pos.println(csvWriter.getHeader());
			auditDao.exportServiceCalls(criteria, cra,
					new ServiceCallRowHandler(pos, csvWriter));
			pos.flush();

		} catch (Exception e) {
			ApplicationException.exception(e,
					CommonMessageID.SERVICE_FAILURE.getMessage());
		} finally {
			try {
				os.flush();
				Closeables.close(os, true);
			} catch (IOException e) {
				ApplicationException.exception(e,
						CommonMessageID.SERVICE_FAILURE.getMessage());
			}
		}
	}

	public CSVWriter getServiceCallCSVWriter() {
		CSVWriterBuilder csvWriterBuilder = new CSVWriterBuilder();
		csvWriterBuilder.addHeader("timestamp");
		csvWriterBuilder.addHeader("user");
		csvWriterBuilder.addHeader("session");
		csvWriterBuilder.addHeader("namespace");
		csvWriterBuilder.addHeader("service");
		csvWriterBuilder.addHeader("success");
		csvWriterBuilder.addHeader("duration");
		csvWriterBuilder.addHeader("payload");
		CSVWriter csvWriter = csvWriterBuilder.build();
		return csvWriter;
	}

	private class ServiceCallRowHandler implements RowHandler<ServiceCall> {
		final PrintStream pos;
		final CSVWriter csvWriter;

		ServiceCallRowHandler(PrintStream pos, CSVWriter csvWriter) {
			this.csvWriter = csvWriter;
			this.pos = pos;
		}

		@Override
		public void handleRow(ServiceCall row) {
			String[] values = { row.getServiceCallTimestamp().toString(),
					row.getUser(), row.getSessionId(), row.getNamespace(),
					row.getService(), row.isSuccess() ? "true" : "false",
					String.valueOf(row.getDuration()), row.getPayload() };
			String line = csvWriter.getLine(values);
			pos.println(line);
		}
	};

	public void setAuditDao(AuditDao auditDao) {
		this.auditDao = auditDao;
	}

}