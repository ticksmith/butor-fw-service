/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.butor.auth.common.AuthModel;
import org.butor.auth.common.firm.Firm;
import org.butor.auth.common.firm.FirmAndUserInfoProvider;
import org.butor.auth.common.user.User;
import org.butor.auth.dao.FirmDao;
import org.butor.auth.dao.UserDao;
import org.butor.json.CommonRequestArgs;
import org.butor.json.util.CommonRequestArgsFactory;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader.InvalidCacheLoadException;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

public class CachedFirmAndUserInfoProvider implements FirmAndUserInfoProvider {
	private Logger logger = LoggerFactory.getLogger(getClass());

	private FirmDao firmDao;
	private UserDao userDao;
	private String system;
	private String function;
	private AuthModel authModel;
	private CommonRequestArgsFactory craFactory;

	public CachedFirmAndUserInfoProvider(FirmDao firmDao, UserDao userDao,
		String system, String function, AuthModel authModel,
		CommonRequestArgsFactory craFactory) {
		this.firmDao = checkNotNull(firmDao);
		this.userDao = checkNotNull(userDao);
		this.system= checkNotNull(system);
		this.function= checkNotNull(function);
		this.authModel = checkNotNull(authModel);
		this.craFactory = checkNotNull(craFactory);
	}
	private Cache<String, User> userCache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.MINUTES).removalListener(new RemovalListener<String, User>() {
		@Override
		public void onRemoval(RemovalNotification<String, User> notification) {
			logger.info("Removing item from cache. user : {} cause : {}",notification.getKey(),notification.getCause()); 
		}
	}).build();
	private Cache<String, Firm> firmCache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.MINUTES).removalListener(new RemovalListener<String, Firm>() {
		@Override
		public void onRemoval(RemovalNotification<String, Firm> notification) {
			logger.info("Removing item from cache. firm : {} cause : {}",notification.getKey(),notification.getCause()); 
		}
	}).build();

	protected Firm getCachedFirm(final long firmId, final String sys, 
			final String func, final AccessMode mode, final CommonRequestArgs cra) {
		try {
			String key = String.format("%s.%s,%s", firmId, sys, func,mode.value());
			Firm f = firmCache.get(key, new Callable<Firm>() {
				@Override
				public Firm call() throws Exception {
					logger.info("Loading firm in cache : {}", firmId);
					return firmDao.readFirm(firmId, sys, func, mode, cra);
				}
			});
			return f;
		} catch (InvalidCacheLoadException e) {
			// no entry found by call()!
			return null;
		} catch (ExecutionException e) {
			throw ApplicationException.exception(e, CommonMessageID.SERVICE_FAILURE.getMessage());
		}
	}
	protected User getCachedUser(final String userId, final CommonRequestArgs cra) {
		try {
			User u = userCache.get(userId, new Callable<User>() {
				@Override
				public User call() throws Exception {
					logger.info("Loading user in cache : {}",userId);
					return userDao.readUser(userId, null, cra);
				}
			});
			return u;
		} catch (InvalidCacheLoadException e) {
			// no entry found by call()!
			return null;
		} catch (ExecutionException e) {
			throw ApplicationException.exception(e, CommonMessageID.SERVICE_FAILURE.getMessage());
		}
	}

	@Override
	public Firm getFirm(long firmId) {
		return getCachedFirm(firmId, system, function, AccessMode.READ, craFactory.create());
	}

	@Override
	public User getUser(String userId) {
		return getCachedUser(userId, craFactory.create());
	}
}
