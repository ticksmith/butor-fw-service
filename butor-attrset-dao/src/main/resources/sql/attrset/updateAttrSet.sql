--
-- Copyright 2013-2017 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

UPDATE attrSet
SET
	id = :id,
	k1 = :k1,
	k2 = :k2,
	revNo = :revNo + 1,
	seq = :seq,
	stamp = CURRENT_TIMESTAMP,
	type = :type,
	userId = :userId,
	value = :value
WHERE
	type = :type AND
	id = :id AND
	k1 = :k1 AND
	k2 = :k2 AND
	revNo = :revNo

