/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.dao;

import java.util.ArrayList;
import java.util.List;

import org.butor.attrset.common.AttrSet;
import org.butor.dao.SQLGenerator;
import org.junit.Test;

public class GenSql {

	
	@Test
	public void generateAttrSetSQL() {
		SQLGenerator sq = new SQLGenerator("REMOVE", "attrSet", AttrSet.class);
		List<String> wc = new ArrayList<String>();
		wc.add("type = :type AND");
		wc.add("id = :id AND"); 
		wc.add("(:k1 IS NULL || k1 = :k1) AND");
		wc.add("(:k2 IS NULL || k2 = :k2)");
		System.out.println(sq.generateSelect(wc));
		System.out.println(sq.generateInsert());
		System.out.println(sq.generateDelete(wc));
		wc.clear();
		wc.add("type = :type AND");
		wc.add("id = :id AND"); 
		wc.add("(:k1 IS NULL || k1 = :k1) AND");
		wc.add("(:k2 IS NULL || k2 = :k2) AND");
		wc.add("revNo = :revNo");
		System.out.println(sq.generateUpdate(wc));
		
	}


}
