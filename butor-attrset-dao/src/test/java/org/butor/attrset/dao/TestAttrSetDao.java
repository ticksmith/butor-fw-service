/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.dao;

import static org.butor.test.ButorTestUtil.getCommonRequestArgs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.attrset.common.AttrSet;
import org.butor.attrset.common.AttrSetCriteria;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.butor.utils.DiffUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.ResourceUtils;

//@RunWith(value=SpringJUnit4ClassRunner.class)
//@ContextConfiguration(value="classpath:test-context.xml")
public class TestAttrSetDao {

	

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}
	
	
	
	@Resource
	DataSource dataSource;
	
	@Resource(name="attrSetDaoTest")
	AttrSetDao attrSetDao;
	
	ButorTestUtil testUtil = new ButorTestUtil();

	@Before 
	public void init() throws Exception{
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		testUtil.executeSQL(jt, ResourceUtils.getURL("classpath:attrset-db.sql"),"");
	}

	//@Test
	public void test() {
			CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
			AttrSet attr = new AttrSet();
			attr.setType("codeset");
			attr.setId("category");
			attr.setK1("mm");
			attr.setSeq(1);
			attr.setK2("en");
			attr.setValue("Money market");

			// insert the attribute in the database
			AttrSet newAttr = attrSetDao.insertAttrSet(attr, cra);
			Object[] diff = DiffUtils.getDifferences(attr, newAttr).toArray();
			Assert.assertArrayEquals(new String[] {"stamp","userId"},diff);
			newAttr.setValue("Titi");

			//update the attribute in the database and compare with the original (not yet inserted) version 
			// and the inserted version. Validate field differences. 

			AttrSet updatedAttrSet  =attrSetDao.updateAttrSet(newAttr, cra);
			diff = DiffUtils.getDifferences(updatedAttrSet, attr).toArray();
			Assert.assertArrayEquals(new String[] {"revNo","stamp","userId","value"},diff);
			diff = DiffUtils.getDifferences(updatedAttrSet, newAttr).toArray();
			Assert.assertArrayEquals(new String[] {"revNo","stamp"},diff);
			
			//read from the database and compare with the updated version
			AttrSetCriteria criteria = AttrSetCriteria.valueOf(attr.getType(), attr.getId(), attr.getK1(), attr.getK2());
			newAttr = attrSetDao.readAttrSet(criteria, cra);
			assertTrue(DiffUtils.getDifferences(newAttr, updatedAttrSet).isEmpty());

			//delete
			attrSetDao.deleteAttrSet(criteria, cra);
			// must be null now
			newAttr = attrSetDao.readAttrSet(criteria, cra);
			assertNull(newAttr);
			
			// insert two rows
			newAttr = attrSetDao.insertAttrSet(attr, cra);
			newAttr.setK2("fr");
			newAttr = attrSetDao.insertAttrSet(newAttr,cra);
			// list them
			criteria.setK2(null);
			List<AttrSet> list = attrSetDao.getAttrSet(criteria, cra);
			assertEquals(2,list.size());
			
	}

}
