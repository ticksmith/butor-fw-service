/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.notif;

import java.util.Date;
import java.util.Map;

import org.butor.utils.ArgsBuilder;
import org.butor.utils.CommonDateFormat;

import com.google.common.collect.Maps;


public class Notification {
	private String type;
	private String name;
	private String to;
	private String stamp;
	private Map<String, Object> data;
	public Notification(String type) {
		this(type, null, null);
	}
	public Notification(String type, String name) {
		this(type, name, null);
	}
	public Notification(String type, String name, String to) {
		this.type = type;
		this.name = name;
		this.to = to;
		this.stamp = CommonDateFormat.YYYYMMDD_HHMMSS_WITHMS.format(new Date());
	}
	public String getType() {
		return type;
	}
	public Notification setType(String type) {
		this.type = type;
		return this;
	}
	public Map<String, Object> getData() {
		if (data == null) {
			data = Maps.newHashMap();
		}
		return data;
	}
	public Notification setData(Map<String, Object> data) {
		getData().putAll(data);
		return this;
	}
	public String getName() {
		return name;
	}
	public Notification setName(String name) {
		this.name = name;
		return this;
	}
	
	public static Notification createNotif(String type, String name, String to) {
		Notification notif = new Notification(type, name, to);
		return notif;
	}
	public Notification setData(String key, Object value) {
		if (data == null) {
			ArgsBuilder ab = ArgsBuilder.create()
					.set("time", CommonDateFormat.YYYYMMDD_HHMMSS.format(new Date()));
			data = ab.build();
		}
		data.put(key, value);
		return this;
	}
	@Override
	public String toString() {
		return "Notification [type=" + type + ", name=" + name + ", to=" + to + ", stamp=" + stamp + ", data=" + data
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((stamp == null) ? 0 : stamp.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notification other = (Notification) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (stamp == null) {
			if (other.stamp != null)
				return false;
		} else if (!stamp.equals(other.stamp))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	public String getTo() {
		return to;
	}
	public Notification setTo(String to) {
		this.to = to;
		return this;
	}
	public String getStamp() {
		return stamp;
	}
	public Notification setStamp(String stamp) {
		this.stamp = stamp;
		return this;
	}

}
