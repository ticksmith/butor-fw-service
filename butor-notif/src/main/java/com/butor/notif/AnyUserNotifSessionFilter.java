/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.notif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * allow notification to be sent to any client.
 * all notifications will be sent to all clients.
 * 
 * this filter should be used only for demo or prototyping because it 
 * does not apply any security/filtering check up.
 * 
 * @author asawan
 */
public class AnyUserNotifSessionFilter implements NotifSessionFilter {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String errMsg = ": ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION\n" +
		"this a demonstration filter that is not intended to production use\n" +
		"It allow a notification to be sent to any user without any security check!\n" +
		"You sould write your own filter that implement required security policies on notifications";
	
	public AnyUserNotifSessionFilter() {
		System.err.println(this.getClass().getName() +errMsg);
		logger.error(errMsg);
	}
	/**
	 * Check if a Notification session (a client) is allowed to receive
	 * a notification message
	 * 
	 * @param notif Notification
	 * @param session NotifSession
	 * @return true if the session is allowed to receive the notification.
	 */
	@Override
	public boolean accept(Notification notif, NotifSession session) {
		System.err.println(this.getClass().getName() +errMsg);
		logger.error(errMsg);
		return true;
	}
}
