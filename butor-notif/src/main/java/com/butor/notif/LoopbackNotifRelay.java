/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.notif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.EventBus;

/**
 * Just to demonstrate the simplest notification relay. The loopback relay.
 * This relay is not really useful especially in a distributed environment.
 * 
 * Instead of posting message via a producer (topic for example), process
 * them like if they we received from a consumer (a topic for example).
 * 
 * So any notification received from a client notif session will be 
 * sent to applicable clients right away without passing via producer/consumer
 * 
 * @author asawan
 * 
 */
public class LoopbackNotifRelay extends AbstractNotifRelay {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public LoopbackNotifRelay(EventBus eventBus) {
		super(eventBus);
		eventBus.register(this);
	}
	
	/**
	 * post into topic a notification message
	 */
	@Override
	public void postMessage(final String serializedClientNotif) {
		if (serializedClientNotif == null) {
			return;
		}
		try {
			processInboundMessage(serializedClientNotif);
		} catch (Exception e) {
			logger.warn("failed while posting message!");
		}
	}
}
